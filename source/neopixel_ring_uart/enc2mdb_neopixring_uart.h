/*
 * enc2mdb_neopixring_uart.h
 *
 *  Created on: 21 Oct 2021
 *      Author: dario
 */

#ifndef NEOPIXEL_RING_UART_ENC2MDB_NEOPIXRING_UART_H_
#define NEOPIXEL_RING_UART_ENC2MDB_NEOPIXRING_UART_H_

extern void enc2mdb_neopixring_uart_send_jogger_event(uint8_t event);
extern void enc2mdb_neopixring_uart_send_encoder_event(uint8_t event, uint8_t rotatingPixelNum);
extern void enc2mdb_neopixring_uart_set_color(uint8_t rColorComp, uint8_t gColorComp, uint8_t bColorComp);
extern void enc2mdb_neopixring_uart_set_mode(uint8_t mode, uint8_t param);
extern void enc2mdb_neopixring_uart_set_speed(uint8_t speed);
extern void enc2mdb_neopixring_uart_set_pixel_on_off(uint8_t pixel, bool pixelOn);
extern void enc2mdb_neopixring_uart_set_all_pixel_on_off(uint8_t *onOffPixels, uint8_t numOfPixels);
extern void enc2mdb_neopixring_uart_set_pixel_color(uint8_t pixel, uint8_t rColorComp, uint8_t gColorComp, uint8_t bColorComp);
extern void enc2mdb_neopixring_uart_set_all_odd_pixel_color(ENC2MDB_RGB_VALUE *pixelsColor, uint8_t numOfPixels);
extern void enc2mdb_neopixring_uart_set_all_even_pixel_color(ENC2MDB_RGB_VALUE *pixelsColor, uint8_t numOfPixels);
extern void enc2mdb_neopixring_uart_send_fw_ver_req();
extern bool enc2mdb_neopixring_uart_is_fw_ver_resp_arrived();
extern void enc2mdb_neopixring_uart_get_fw_ver(uint8_t *major, uint8_t *minor, uint8_t *patch);
extern void enc2mdb_neopixring_uart_init();

#endif /* NEOPIXEL_RING_UART_ENC2MDB_NEOPIXRING_UART_H_ */
