/*
 * enc2mdb_neopixring_uart.c
 *
 *  Created on: 21 Oct 2021
 *      Author: dario
 */

#include <stdint.h>
#include "fsl_uart.h"
#include "fsl_debug_console.h"
#include "enc2mdb_types.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define UART_NEOPIXRING          	UART1
#define UART_NEOPIXRING_CLK_FREQ 	CLOCK_GetFreq(UART1_CLK_SRC)
#define UART_NEOPIXRING_BAUDRATE 	9600
#define TX_BUFF_LEN					50U
#define RX_BUFF_LEN					20U
#define PIXONOFF_MSG_LEN			28U
#define PIXCOLORS_MSG_LEN			40U

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
/* UART user callback */
void UART_FwVersionCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData);

/*******************************************************************************
 * Variables
 ******************************************************************************/
uint8_t txBuff[TX_BUFF_LEN] = {0U};
uint8_t rxBuff[RX_BUFF_LEN] = {0U};
uint8_t rxFwVerBuff[RX_BUFF_LEN] = {0U};
volatile bool g_FwVersionRespArrived = false;

static const char g_EventCmd = 'e';
static const char g_SetColorCmd = 'c';
static const char g_SetSinglePixColorCmd = 'x';
static const char g_SetSinglePixOddColorCmd = 'o';
static const char g_SetSinglePixEvenColorCmd = 'v';
static const char g_SetModeCmd = 'm';
static const char g_SetPixOnOffCmd = 'p';
static const char g_SetAllPixOnOffCmd = 'l';
static const char g_SetSpeedCmd = 's';
static const char g_GetFwVerCmd = 'f';
uart_handle_t g_UartHandle;

/*******************************************************************************
 * Code
 ******************************************************************************/
extern void UART1_DriverIRQHandler(void);

void UART_FwVersionCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData)
{
    userData = userData;

    if (kStatus_UART_RxIdle == status)
        g_FwVersionRespArrived = true;
}


void enc2mdb_neopixring_uart_send_jogger_event(uint8_t event)
{
	uint8_t i;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)3U;
	txBuff[2] = (uint8_t)g_EventCmd;
	txBuff[3] = event;
	txBuff[4] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, 5);
}


void enc2mdb_neopixring_uart_send_encoder_event(uint8_t event, uint8_t rotatingPixelNum)
{
	uint8_t i;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)4U;
	txBuff[2] = (uint8_t)g_EventCmd;
	txBuff[3] = event;
	txBuff[4] = rotatingPixelNum;
	txBuff[5] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, 6);
}


void enc2mdb_neopixring_uart_set_color(uint8_t rColorComp, uint8_t gColorComp, uint8_t bColorComp)
{
	uint8_t i;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)5U;
	txBuff[2] = (uint8_t)g_SetColorCmd;
	txBuff[3] = rColorComp;
	txBuff[4] = gColorComp;
	txBuff[5] = bColorComp;
	txBuff[6] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, 7);
}


void enc2mdb_neopixring_uart_set_mode(uint8_t mode, uint8_t param)
{
	uint8_t i;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)4U;
	txBuff[2] = (uint8_t)g_SetModeCmd;
	txBuff[3] = mode;
	txBuff[4] = param;
	txBuff[5] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, 6);
}


void enc2mdb_neopixring_uart_set_speed(uint8_t speed)
{
	uint8_t i;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)3U;
	txBuff[2] = (uint8_t)g_SetSpeedCmd;
	txBuff[3] = speed;
	txBuff[4] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, 5);
}


void enc2mdb_neopixring_uart_set_pixel_on_off(uint8_t pixel, bool pixelOn)
{
	uint8_t i;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)4U;
	txBuff[2] = (uint8_t)g_SetPixOnOffCmd;
	txBuff[3] = pixel;
	txBuff[4] = pixelOn;
	txBuff[5] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, 6);
}


void enc2mdb_neopixring_uart_set_all_pixel_on_off(uint8_t *onOffPixels, uint8_t numOfPixels)
{
	uint8_t i;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)(PIXONOFF_MSG_LEN-2U);
	txBuff[2] = (uint8_t)g_SetAllPixOnOffCmd;
	for(i = 0; i < numOfPixels; ++i) {
		txBuff[i+3] = onOffPixels[i];
	}
	txBuff[PIXONOFF_MSG_LEN-1] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, PIXONOFF_MSG_LEN);
}


void enc2mdb_neopixring_uart_set_pixel_color(uint8_t pixel, uint8_t rColorComp, uint8_t gColorComp, uint8_t bColorComp)
{
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)6U;
	txBuff[2] = (uint8_t)g_SetSinglePixColorCmd;
	txBuff[3] = pixel;
	txBuff[4] = rColorComp;
	txBuff[5] = gColorComp;
	txBuff[6] = bColorComp;
	txBuff[7] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, 8);
}


void enc2mdb_neopixring_uart_set_all_odd_pixel_color(ENC2MDB_RGB_VALUE *pixelsColor, uint8_t numOfPixels)
{
	uint8_t i, j = 3;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)(PIXCOLORS_MSG_LEN-2U);
	txBuff[2] = (uint8_t)g_SetSinglePixOddColorCmd;
	for(i = 1; i < numOfPixels; i+=2) {
		txBuff[j] = (uint8_t) pixelsColor[i].colorR;
		txBuff[j+1] = (uint8_t) pixelsColor[i].colorG;
		txBuff[j+2] = (uint8_t) pixelsColor[i].colorB;
		j += 3;
	}
	txBuff[PIXCOLORS_MSG_LEN-1] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, PIXCOLORS_MSG_LEN);
}


void enc2mdb_neopixring_uart_set_all_even_pixel_color(ENC2MDB_RGB_VALUE *pixelsColor, uint8_t numOfPixels)
{
	uint8_t i, j = 3;
	txBuff[0] = '$';
	txBuff[1] = (uint8_t)(PIXCOLORS_MSG_LEN-2U);
	txBuff[2] = (uint8_t)g_SetSinglePixEvenColorCmd;
	for(i = 0; i < numOfPixels; i+=2) {
		txBuff[j] = (uint8_t) pixelsColor[i].colorR;
		txBuff[j+1] = (uint8_t) pixelsColor[i].colorG;
		txBuff[j+2] = (uint8_t) pixelsColor[i].colorB;
		j += 3;
	}
	txBuff[PIXCOLORS_MSG_LEN-1] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, PIXCOLORS_MSG_LEN);
}


void enc2mdb_neopixring_uart_send_fw_ver_req()
{
    int i;
    status_t resp;
    uart_transfer_t receiveXfer;

    /* Start to echo. */
    receiveXfer.data     = rxFwVerBuff;
    receiveXfer.dataSize = 3;

	txBuff[0] = '$';
	txBuff[1] = (uint8_t)2U;
	txBuff[2] = (uint8_t)g_GetFwVerCmd;
	txBuff[3] = '#';
	UART_WriteBlocking(UART_NEOPIXRING, txBuff, 4);
	UART_TransferReceiveNonBlocking(UART_NEOPIXRING, &g_UartHandle, &receiveXfer, NULL);
}


bool enc2mdb_neopixring_uart_is_fw_ver_resp_arrived()
{
    return g_FwVersionRespArrived;
}


void enc2mdb_neopixring_uart_get_fw_ver(uint8_t *major, uint8_t *minor, uint8_t *patch)
{
	*major = rxFwVerBuff[0];
	*minor = rxFwVerBuff[1];
	*patch = rxFwVerBuff[2];
}


void enc2mdb_neopixring_uart_init()
{
    uart_config_t config;
    int i;

    UART_GetDefaultConfig(&config);
    config.baudRate_Bps = UART_NEOPIXRING_BAUDRATE;
    config.enableTx     = true;
    config.enableRx     = true;

    UART_Init(UART_NEOPIXRING, &config, UART_NEOPIXRING_CLK_FREQ);
    UART_TransferCreateHandle(UART_NEOPIXRING, &g_UartHandle, UART_FwVersionCallback, NULL);
}
