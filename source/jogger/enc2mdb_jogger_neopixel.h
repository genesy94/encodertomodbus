/*
 * enc2mdb_encoder.h
 *
 *  Created on: 1 Oct 2021
 *      Author: dario
 */

#ifndef JOGGER_ENC2MDB_JOGGER_NEOPIXEL_H_
#define JOGGER_ENC2MDB_JOGGER_NEOPIXEL_H_


typedef void (*ENC2MDB_WRITE_COMMAND_FUNC)(uint16_t, uint16_t);

extern void enc2mdb_jogger_initNeopixel();

extern uint8_t enc2mdb_jogger_getNeopixelNumOfPixels();

extern uint16_t enc2mdb_jogger_getNeopixelRingFwMajor(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRingFwMajor(uint16_t major);

extern uint16_t enc2mdb_jogger_getNeopixelRingFwMinor(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRingFwMinor(uint16_t minor);

extern uint16_t enc2mdb_jogger_getNeopixelRingFwPatch(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRingFwPatch(uint16_t patch);

extern uint16_t enc2mdb_jogger_getNeopixelRingColorR(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRingColorR(uint16_t colorR, uint16_t unusedPar);

extern uint16_t enc2mdb_jogger_getNeopixelRingColorG(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRingColorG(uint16_t colorG, uint16_t unusedPar);

extern uint16_t enc2mdb_jogger_getNeopixelRingColorB(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRingColorB(uint16_t colorB, uint16_t unusedPar);

extern uint16_t enc2mdb_jogger_getNeopixelRingEffectiveColorR(uint16_t unusedPar);
extern uint16_t enc2mdb_jogger_getNeopixelRingEffectiveColorG(uint16_t unusedPar);
extern uint16_t enc2mdb_jogger_getNeopixelRingEffectiveColorB(uint16_t unusedPar);

extern uint16_t enc2mdb_jogger_getNeopixelRingMode(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRingMode(uint16_t mode, uint16_t unusedPar);

extern uint16_t enc2mdb_jogger_getNeopixelRingSpeed(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRingSpeed(uint16_t speed, uint16_t unusedPar);

extern uint16_t enc2mdb_jogger_getNeopixelDimmingValue(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelDimmingValue(uint16_t dimmingValue, uint16_t unusedPar);

extern uint16_t enc2mdb_jogger_getNeopixelRotatingPixel(uint16_t unusedPar);
extern void enc2mdb_jogger_setNeopixelRotatingPixel(uint16_t pixel, uint16_t unusedPar);

extern uint16_t enc2mdb_jogger_getNeopixelSinglePixelOn(uint16_t valueIndex);
extern void enc2mdb_jogger_setNeopixelSinglePixelOn(uint16_t value, uint16_t valueIndex);

extern uint16_t enc2mdb_jogger_getNeopixelSinglePixelColorR(uint16_t valueIndex);
extern void enc2mdb_jogger_setNeopixelSinglePixelColorR(uint16_t value, uint16_t valueIndex);

extern uint16_t enc2mdb_jogger_getNeopixelSinglePixelColorG(uint16_t valueIndex);
extern void enc2mdb_jogger_setNeopixelSinglePixelColorG(uint16_t value, uint16_t valueIndex);

extern uint16_t enc2mdb_jogger_getNeopixelSinglePixelColorB(uint16_t valueIndex);
extern void enc2mdb_jogger_setNeopixelSinglePixelColorB(uint16_t value, uint16_t valueIndex);

extern uint16_t enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorR(uint16_t valueIndex);
extern uint16_t enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorG(uint16_t valueIndex);
extern uint16_t enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorB(uint16_t valueIndex);


#endif /* JOGGER_ENC2MDB_JOGGER_NEOPIXEL_H_ */
