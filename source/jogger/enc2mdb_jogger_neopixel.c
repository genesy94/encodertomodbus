/*
 * enc2mdb_encoder.c
 *
 *  Created on: 1 Oct 2021
 *      Author: dario
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <enc2mdb_types.h>
#include "enc2mdb_neopixring_uart.h"
#include "PacketBuffer.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define MAX_RGB_VALUE			255U
#define	MAX_PIXEL_VALUE			23U
#define UP_REF_PIXEL_NUM		0U
#define MAX_DIMMING_VALUE		100U
#define SINGLE_PIXEL_ON_VALUE	1U
#define INIT_COLOR_R			0
#define INIT_COLOR_G			100
#define INIT_COLOR_B			100


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static ENC2MDB_RGB_VALUE g_NeopixelRegistersColorRGB = {(int16_t)INIT_COLOR_R, (int16_t)INIT_COLOR_G, (int16_t)INIT_COLOR_B};
static ENC2MDB_RGB_VALUE g_NeopixelCurrentColorRGB = {(int16_t)INIT_COLOR_R, (int16_t)INIT_COLOR_G, (int16_t)INIT_COLOR_B};
static ENC2MDB_RGB_DIMMING_STEP g_NeopixelDimmingStepRGB = {((double)INIT_COLOR_R/MAX_DIMMING_VALUE), ((double)INIT_COLOR_G / MAX_DIMMING_VALUE), ((double)INIT_COLOR_B / MAX_DIMMING_VALUE)};
static enum NEOPIXEL_RING_MODE g_NeopixelMode = modeFollowMoveOneColor;
static enum NEOPIXEL_RING_SPEED g_NeopixelSpeed = speedNormal;
static uint16_t g_NeopixelDimmingValue = MAX_DIMMING_VALUE;
static uint16_t g_NeopixelFwMajor = 0U;
static uint16_t g_NeopixelFwMinor = 0U;
static uint16_t g_NeopixelFwPatch = 0U;
static uint16_t g_NeopixelRotatingPixel = UP_REF_PIXEL_NUM;
static uint16_t g_NeopixelSinglePixelOn[MAX_PIXEL_VALUE + 1] = {0U};
static ENC2MDB_RGB_VALUE g_NeopixelSinglePixelRegistersColorRGB[MAX_PIXEL_VALUE + 1];
static ENC2MDB_RGB_VALUE g_NeopixelSinglePixelCurrentColorRGB[MAX_PIXEL_VALUE + 1];
static ENC2MDB_RGB_DIMMING_STEP g_NeopixelSinglePixelDimmingStepRGB[MAX_PIXEL_VALUE + 1];
static enum MDB_EVENT g_EventToSend = noEvent;


/*******************************************************************************
 * Code
 ******************************************************************************/
static int16_t singleColorComponentDimming(int16_t colorValue, double dimmingStep)
{
	int16_t dimmingDiff, result;
	double tempResult;
	dimmingDiff = MAX_DIMMING_VALUE - g_NeopixelDimmingValue;
	tempResult = (double)(colorValue - (dimmingDiff * dimmingStep));
	result = (tempResult >= 0.0) ? (int16_t)ceil(tempResult) : (int16_t)floor(tempResult);
	return result;
}


static void rgbDimming(uint16_t dimmingValue, ENC2MDB_RGB_VALUE *rgbValue, ENC2MDB_RGB_DIMMING_STEP *rgbDimmingStep)
{
	int16_t dimmingDiff, addColorR, addColorG, addColorB;
	double tempColorR, tempColorG, tempColorB;
	dimmingDiff = dimmingValue - g_NeopixelDimmingValue;
	tempColorR = (double)(dimmingDiff * rgbDimmingStep->dimmingStepR);
	tempColorG = (double)(dimmingDiff * rgbDimmingStep->dimmingStepG);
	tempColorB = (double)(dimmingDiff * rgbDimmingStep->dimmingStepB);
	addColorR = (tempColorR >= 0.0) ? (int16_t)ceil(tempColorR) : (int16_t)floor(tempColorR);
	addColorG = (tempColorG >= 0.0) ? (int16_t)ceil(tempColorG) : (int16_t)floor(tempColorG);
	addColorB = (tempColorB >= 0.0) ? (int16_t)ceil(tempColorB) : (int16_t)floor(tempColorB);
	rgbValue->colorR += addColorR;
	rgbValue->colorG += addColorG;
	rgbValue->colorB += addColorB;
}


void enc2mdb_jogger_initNeopixel()
{
	uint8_t i;
	for(i = 0U; i < MAX_PIXEL_VALUE + 1; ++i) {
		g_NeopixelSinglePixelOn[i] = 1U;
		g_NeopixelSinglePixelRegistersColorRGB[i].colorR = (int16_t)INIT_COLOR_R;
		g_NeopixelSinglePixelRegistersColorRGB[i].colorG = (int16_t)INIT_COLOR_G;
		g_NeopixelSinglePixelRegistersColorRGB[i].colorB = (int16_t)INIT_COLOR_B;
		g_NeopixelSinglePixelCurrentColorRGB[i].colorR = (int16_t)INIT_COLOR_R;
		g_NeopixelSinglePixelCurrentColorRGB[i].colorG = (int16_t)INIT_COLOR_G;
		g_NeopixelSinglePixelCurrentColorRGB[i].colorB = (int16_t)INIT_COLOR_B;
		g_NeopixelSinglePixelDimmingStepRGB[i].dimmingStepR = (double)INIT_COLOR_R / MAX_DIMMING_VALUE;
		g_NeopixelSinglePixelDimmingStepRGB[i].dimmingStepG = (double)INIT_COLOR_G / MAX_DIMMING_VALUE;
		g_NeopixelSinglePixelDimmingStepRGB[i].dimmingStepB = (double)INIT_COLOR_B / MAX_DIMMING_VALUE;
	}
}


uint8_t enc2mdb_jogger_getNeopixelNumOfPixels()
{
	return (uint8_t)(MAX_PIXEL_VALUE + 1U);
}


uint16_t enc2mdb_jogger_getNeopixelRingColorR(uint16_t unusedPar)
{
	(void) unusedPar;
	if(g_NeopixelRegistersColorRGB.colorR > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelRegistersColorRGB.colorR < 0)
		return 0U;
	return (uint16_t)g_NeopixelRegistersColorRGB.colorR;
}


void enc2mdb_jogger_setNeopixelRingColorR(uint16_t colorR, uint16_t unusedPar)
{
	(void) unusedPar;
	if(colorR > MAX_RGB_VALUE)
		g_NeopixelRegistersColorRGB.colorR = (int16_t)MAX_RGB_VALUE;
	else
		g_NeopixelRegistersColorRGB.colorR = (int16_t)colorR;
	g_NeopixelDimmingStepRGB.dimmingStepR = (double)g_NeopixelRegistersColorRGB.colorR / MAX_DIMMING_VALUE;
	g_NeopixelCurrentColorRGB.colorR = singleColorComponentDimming(g_NeopixelRegistersColorRGB.colorR, g_NeopixelDimmingStepRGB.dimmingStepR);
	g_EventToSend = generalColor;
	PBUF_addMdbEventPacket(&g_EventToSend);
}


uint16_t enc2mdb_jogger_getNeopixelRingColorG(uint16_t unusedPar)
{
	(void) unusedPar;
	if(g_NeopixelRegistersColorRGB.colorG > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelRegistersColorRGB.colorG < 0)
		return 0U;
	return (uint16_t)g_NeopixelRegistersColorRGB.colorG;
}


void enc2mdb_jogger_setNeopixelRingColorG(uint16_t colorG, uint16_t unusedPar)
{
	(void) unusedPar;
	if(colorG > MAX_RGB_VALUE)
		g_NeopixelRegistersColorRGB.colorG = (int16_t)MAX_RGB_VALUE;
	else
		g_NeopixelRegistersColorRGB.colorG = (int16_t)colorG;
	g_NeopixelDimmingStepRGB.dimmingStepG = (double)g_NeopixelRegistersColorRGB.colorG / MAX_DIMMING_VALUE;
	g_NeopixelCurrentColorRGB.colorG = singleColorComponentDimming(g_NeopixelRegistersColorRGB.colorG, g_NeopixelDimmingStepRGB.dimmingStepG);
	g_EventToSend = generalColor;
	PBUF_addMdbEventPacket(&g_EventToSend);
}


uint16_t enc2mdb_jogger_getNeopixelRingColorB(uint16_t unusedPar)
{
	(void) unusedPar;
	if(g_NeopixelRegistersColorRGB.colorB > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelRegistersColorRGB.colorB < 0)
		return 0U;
	return (uint16_t)g_NeopixelRegistersColorRGB.colorB;
}


void enc2mdb_jogger_setNeopixelRingColorB(uint16_t colorB, uint16_t unusedPar)
{
	(void) unusedPar;
	if(colorB > MAX_RGB_VALUE)
		g_NeopixelRegistersColorRGB.colorB = (int16_t)MAX_RGB_VALUE;
	else
		g_NeopixelRegistersColorRGB.colorB = (int16_t)colorB;
	g_NeopixelDimmingStepRGB.dimmingStepB = (double)g_NeopixelRegistersColorRGB.colorB / MAX_DIMMING_VALUE;
	g_NeopixelCurrentColorRGB.colorB = singleColorComponentDimming(g_NeopixelRegistersColorRGB.colorB, g_NeopixelDimmingStepRGB.dimmingStepB);
	g_EventToSend = generalColor;
	PBUF_addMdbEventPacket(&g_EventToSend);
}


uint16_t enc2mdb_jogger_getNeopixelRingEffectiveColorR(uint16_t unusedPar)
{
	(void) unusedPar;
	if(g_NeopixelCurrentColorRGB.colorR > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelCurrentColorRGB.colorR < 0)
		return 0U;
	return (uint16_t)g_NeopixelCurrentColorRGB.colorR;
}


uint16_t enc2mdb_jogger_getNeopixelRingEffectiveColorG(uint16_t unusedPar)
{
	(void) unusedPar;
	if(g_NeopixelCurrentColorRGB.colorG > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelCurrentColorRGB.colorG < 0)
		return 0U;
	return (uint16_t)g_NeopixelCurrentColorRGB.colorG;
}


uint16_t enc2mdb_jogger_getNeopixelRingEffectiveColorB(uint16_t unusedPar)
{
	(void) unusedPar;
	if(g_NeopixelCurrentColorRGB.colorB > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelCurrentColorRGB.colorB < 0)
		return 0U;
	return (uint16_t)g_NeopixelCurrentColorRGB.colorB;
}


uint16_t enc2mdb_jogger_getNeopixelRingMode(uint16_t unusedPar)
{
	(void) unusedPar;
	return (uint16_t)g_NeopixelMode;
}


void enc2mdb_jogger_setNeopixelRingMode(uint16_t mode, uint16_t unusedPar)
{
	(void) unusedPar;
	if(mode > modeFlashingColorPerPixel)
		g_NeopixelMode = modeFlashingColorPerPixel;
	else
		g_NeopixelMode = mode;
	g_EventToSend = jogMode;
	PBUF_addMdbEventPacket(&g_EventToSend);
}


uint16_t enc2mdb_jogger_getNeopixelRingSpeed(uint16_t unusedPar)
{
	(void) unusedPar;
	return (uint16_t)g_NeopixelSpeed;
}


void enc2mdb_jogger_setNeopixelRingSpeed(uint16_t speed, uint16_t unusedPar)
{
	(void) unusedPar;
	if(speed > speedFast)
		g_NeopixelSpeed = speedFast;
	else
		g_NeopixelSpeed = speed;
	g_EventToSend = jogSpeed;
	PBUF_addMdbEventPacket(&g_EventToSend);
}


uint16_t enc2mdb_jogger_getNeopixelDimmingValue(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_NeopixelDimmingValue;
}


void enc2mdb_jogger_setNeopixelDimmingValue(uint16_t dimmingValue, uint16_t unusedPar)
{
	uint8_t i;
	(void) unusedPar;
	if(dimmingValue == g_NeopixelDimmingValue)
		return;
	if(dimmingValue > MAX_DIMMING_VALUE)
		dimmingValue = MAX_DIMMING_VALUE;

	rgbDimming(dimmingValue, &g_NeopixelCurrentColorRGB, &g_NeopixelDimmingStepRGB);
	g_EventToSend = generalColor;
	PBUF_addMdbEventPacket(&g_EventToSend);
	for(i = 0U; i <= MAX_PIXEL_VALUE; ++i) {
		rgbDimming(dimmingValue, &g_NeopixelSinglePixelCurrentColorRGB[i], &g_NeopixelSinglePixelDimmingStepRGB[i]);
	}
	g_EventToSend = singleEvenPixelColor;
	PBUF_addMdbEventPacket(&g_EventToSend);
	g_EventToSend = singleOddPixelColor;
	PBUF_addMdbEventPacket(&g_EventToSend);
	g_NeopixelDimmingValue = dimmingValue;
}


uint16_t enc2mdb_jogger_getNeopixelRingFwMajor(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_NeopixelFwMajor;
}


void enc2mdb_jogger_setNeopixelRingFwMajor(uint16_t major)
{
	g_NeopixelFwMajor = major;
}


uint16_t enc2mdb_jogger_getNeopixelRingFwMinor(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_NeopixelFwMinor;
}


void enc2mdb_jogger_setNeopixelRingFwMinor(uint16_t minor)
{
	g_NeopixelFwMinor = minor;
}


uint16_t enc2mdb_jogger_getNeopixelRingFwPatch(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_NeopixelFwPatch;
}


void enc2mdb_jogger_setNeopixelRingFwPatch(uint16_t patch)
{
	g_NeopixelFwPatch = patch;
}


uint16_t enc2mdb_jogger_getNeopixelRotatingPixel(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_NeopixelRotatingPixel;
}


void enc2mdb_jogger_setNeopixelRotatingPixel(uint16_t pixel, uint16_t unusedPar)
{
	(void) unusedPar;
	if(pixel > MAX_PIXEL_VALUE)
		g_NeopixelRotatingPixel = MAX_PIXEL_VALUE;
	else
		g_NeopixelRotatingPixel = pixel;
}


uint16_t enc2mdb_jogger_getNeopixelSinglePixelOn(uint16_t valueIndex)
{
	return g_NeopixelSinglePixelOn[valueIndex];
}


void enc2mdb_jogger_setNeopixelSinglePixelOn(uint16_t value, uint16_t valueIndex)
{
	if(value > SINGLE_PIXEL_ON_VALUE)
		value = SINGLE_PIXEL_ON_VALUE;
	if(valueIndex <= MAX_PIXEL_VALUE)
		g_NeopixelSinglePixelOn[valueIndex] = value;
	g_EventToSend = singlePixelOnOff;
	PBUF_addMdbEventPacket(&g_EventToSend);
}


 uint16_t enc2mdb_jogger_getNeopixelSinglePixelColorR(uint16_t valueIndex)
 {
	if(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorR > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorR < 0)
		return 0U;
	return (uint16_t)g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorR;
 }


 void enc2mdb_jogger_setNeopixelSinglePixelColorR(uint16_t value, uint16_t valueIndex)
 {
	if(value > MAX_RGB_VALUE)
		value = MAX_RGB_VALUE;
	if(valueIndex <= MAX_PIXEL_VALUE) {
		g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorR = (int16_t)value;
		g_NeopixelSinglePixelDimmingStepRGB[valueIndex].dimmingStepR = (double)g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorR / MAX_DIMMING_VALUE;
		g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorR = singleColorComponentDimming(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorR, g_NeopixelSinglePixelDimmingStepRGB[valueIndex].dimmingStepR);
		if(valueIndex%2 != 0)
			g_EventToSend = singleOddPixelColor;
		else
			g_EventToSend = singleEvenPixelColor;
		PBUF_addMdbEventPacket(&g_EventToSend);
	}
 }


 uint16_t enc2mdb_jogger_getNeopixelSinglePixelColorG(uint16_t valueIndex)
 {
	if(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorG > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorG < 0)
		return 0U;
	return (uint16_t)g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorG;
 }


 void enc2mdb_jogger_setNeopixelSinglePixelColorG(uint16_t value, uint16_t valueIndex)
 {
	if(value > MAX_RGB_VALUE)
		value = MAX_RGB_VALUE;
	if(valueIndex <= MAX_PIXEL_VALUE) {
		g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorG = (int16_t)value;
		g_NeopixelSinglePixelDimmingStepRGB[valueIndex].dimmingStepG = (double)g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorG / MAX_DIMMING_VALUE;
		g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorG = singleColorComponentDimming(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorG, g_NeopixelSinglePixelDimmingStepRGB[valueIndex].dimmingStepG);
		if(valueIndex%2 != 0)
			g_EventToSend = singleOddPixelColor;
		else
			g_EventToSend = singleEvenPixelColor;
		PBUF_addMdbEventPacket(&g_EventToSend);
	}
 }


 uint16_t enc2mdb_jogger_getNeopixelSinglePixelColorB(uint16_t valueIndex)
 {
	if(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorB > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorB < 0)
		return 0U;
	return (uint16_t)g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorB;
 }


 void enc2mdb_jogger_setNeopixelSinglePixelColorB(uint16_t value, uint16_t valueIndex)
 {
	if(value > MAX_RGB_VALUE)
		value = MAX_RGB_VALUE;
	if(valueIndex <= MAX_PIXEL_VALUE) {
		g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorB = (int16_t)value;
		g_NeopixelSinglePixelDimmingStepRGB[valueIndex].dimmingStepB = (double)g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorB / MAX_DIMMING_VALUE;
		g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorB = singleColorComponentDimming(g_NeopixelSinglePixelRegistersColorRGB[valueIndex].colorB, g_NeopixelSinglePixelDimmingStepRGB[valueIndex].dimmingStepB);
		if(valueIndex%2 != 0)
			g_EventToSend = singleOddPixelColor;
		else
			g_EventToSend = singleEvenPixelColor;
		PBUF_addMdbEventPacket(&g_EventToSend);
	}
 }


 uint16_t enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorR(uint16_t valueIndex)
 {
	if(g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorR > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorR < 0)
		return 0U;
	return (uint16_t)g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorR;
 }


 uint16_t enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorG(uint16_t valueIndex)
 {
	if(g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorG > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorG < 0)
		return 0U;
	return (uint16_t)g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorG;
 }


 uint16_t enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorB(uint16_t valueIndex)
 {
	if(g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorB > (int16_t)MAX_RGB_VALUE)
		return MAX_RGB_VALUE;
	if(g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorB < 0)
		return 0U;
	return (uint16_t)g_NeopixelSinglePixelCurrentColorRGB[valueIndex].colorB;
 }
