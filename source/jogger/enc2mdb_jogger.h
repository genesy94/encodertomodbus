/*
 * enc2mdb_encoder.h
 *
 *  Created on: 1 Oct 2021
 *      Author: dario
 */

#ifndef JOGGER_ENC2MDB_JOGGER_H_
#define JOGGER_ENC2MDB_JOGGER_H_


typedef uint16_t (*ENC2MDB_READ_FEEDBACK_FUNC)(uint16_t);
typedef void (*ENC2MDB_SET_MOVE_VALUE_FUNC)(uint16_t);
typedef void (*ENC2MDB_SET_FLAG_FUNC)();

extern uint16_t enc2mdb_jogger_getUnusedRegValue(uint16_t index);
extern void enc2mdb_jogger_setUnusedRegValue(uint16_t value, uint16_t index);

extern uint16_t enc2mdb_jogger_getOnlineFlag(uint16_t unusedPar);
extern void enc2mdb_jogger_setOnlineFlag();
extern void enc2mdb_jogger_resetOnlineFlag();

extern uint16_t enc2mdb_jogger_getRotationCounter(uint16_t unusedPar);
extern void enc2mdb_jogger_addRotation(int16_t lastRotationCount);

extern uint16_t enc2mdb_jogger_getPushFlag(uint16_t unusedPar);
extern void enc2mdb_jogger_setPushFlag();
extern void enc2mdb_jogger_resetPushFlag();

extern uint16_t enc2mdb_jogger_getPushCounter(uint16_t unusedPar);
extern void enc2mdb_jogger_addPush(int16_t lastPushCount);

extern uint16_t enc2mdb_jogger_getDoublePushCounter(uint16_t unusedPar);
extern void enc2mdb_jogger_addDoublePush(int16_t lastDoublePushCount);

extern uint16_t enc2mdb_jogger_getMoveLeftFlag(uint16_t unusedPar);
extern void enc2mdb_jogger_setMoveLeftFlag();
extern void enc2mdb_jogger_resetMoveLeftFlag();

extern uint16_t enc2mdb_jogger_getMoveRightFlag(uint16_t unusedPar);
extern void enc2mdb_jogger_setMoveRightFlag();
extern void enc2mdb_jogger_resetMoveRightFlag();

extern uint16_t enc2mdb_jogger_getMoveUpFlag(uint16_t unusedPar);
extern void enc2mdb_jogger_setMoveUpFlag();
extern void enc2mdb_jogger_resetMoveUpFlag();

extern uint16_t enc2mdb_jogger_getMoveDownFlag(uint16_t unusedPar);
extern void enc2mdb_jogger_setMoveDownFlag();
extern void enc2mdb_jogger_resetMoveDownFlag();

extern uint16_t enc2mdb_jogger_getMoveUpValue(uint16_t unusedPar);
extern void enc2mdb_jogger_setMoveUpValue(uint16_t moveValue);

extern uint16_t enc2mdb_jogger_getMoveDownValue(uint16_t unusedPar);
extern void enc2mdb_jogger_setMoveDownValue(uint16_t moveValue);

extern uint16_t enc2mdb_jogger_getMoveLeftValue(uint16_t unusedPar);
extern void enc2mdb_jogger_setMoveLeftValue(uint16_t moveValue);

extern uint16_t enc2mdb_jogger_getMoveRightValue(uint16_t unusedPar);
extern void enc2mdb_jogger_setMoveRightValue(uint16_t moveValue);

extern uint16_t enc2mdb_jogger_getFwMajor(uint16_t unusedPar);
extern uint16_t enc2mdb_jogger_getFwMinor(uint16_t unusedPar);
extern uint16_t enc2mdb_jogger_getFwPatch(uint16_t unusedPar);


#endif /* JOGGER_ENC2MDB_JOGGER_H_ */
