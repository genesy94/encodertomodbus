/*
 * enc2mdb_encoder.c
 *
 *  Created on: 1 Oct 2021
 *      Author: dario
 */

#include <stdint.h>
#include <stdbool.h>
#include <enc2mdb_types.h>
#include <enc2mdb_fw_version.h>


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define MAX_MOVE_VALUE		100U
#define UNUSED_REGS_DIM		200U

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static bool g_OnlineFlag = false;
static volatile uint16_t g_RotationCounter = 0U;
static bool g_PushFlag = false;
static uint16_t g_PushCounter = 0U;
static uint16_t g_DoublePushCounter = 0U;
static bool g_MoveUpFlag = false;
static bool g_MoveDownFlag = false;
static bool g_MoveLeftFlag = false;
static bool g_MoveRightFlag = false;
static uint16_t g_MoveUpValue = 0U;
static uint16_t g_MoveDownValue = 0U;
static uint16_t g_MoveLeftValue = 0U;
static uint16_t g_MoveRightValue = 0U;
static uint16_t g_UnusedRegs[UNUSED_REGS_DIM] = {0U};
const static uint16_t g_FwMajor = (uint16_t)FW_VERSION_MAJOR_NUM;
const static uint16_t g_FwMinor = (uint16_t)FW_VERSION_MINOR_NUM;
const static uint16_t g_FwPatch = (uint16_t)FW_VERSION_PATCH_NUM;

/*******************************************************************************
 * Code
 ******************************************************************************/
uint16_t enc2mdb_jogger_getUnusedRegValue(uint16_t index)
{
	if(index > UNUSED_REGS_DIM - 1)
		index = UNUSED_REGS_DIM - 1;
	return g_UnusedRegs[index];
}


void enc2mdb_jogger_setUnusedRegValue(uint16_t value, uint16_t index)
{
	if(index > UNUSED_REGS_DIM - 1)
		index = UNUSED_REGS_DIM - 1;
	g_UnusedRegs[index] = value;
}


uint16_t enc2mdb_jogger_getOnlineFlag(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_OnlineFlag;
}


void enc2mdb_jogger_setOnlineFlag()
{
	g_OnlineFlag = true;
}


void enc2mdb_jogger_resetOnlineFlag()
{
	g_OnlineFlag = false;
}


uint16_t enc2mdb_jogger_getRotationCounter(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_RotationCounter;
}


void enc2mdb_jogger_addRotation(int16_t lastRotationCount)
{
	g_RotationCounter = (uint16_t)(g_RotationCounter + lastRotationCount);
}


uint16_t enc2mdb_jogger_getPushFlag(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_PushFlag;
}


void enc2mdb_jogger_setPushFlag()
{
	g_PushFlag = true;
}


void enc2mdb_jogger_resetPushFlag()
{
	g_PushFlag = false;
}


uint16_t enc2mdb_jogger_getPushCounter(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_PushCounter;
}


void enc2mdb_jogger_addPush(int16_t lastPushCount)
{
	g_PushCounter = (uint16_t)(g_PushCounter + lastPushCount);
}


uint16_t enc2mdb_jogger_getDoublePushCounter(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_DoublePushCounter;
}


void enc2mdb_jogger_addDoublePush(int16_t lastDoublePushCount)
{
	g_DoublePushCounter = (uint16_t)(g_DoublePushCounter + lastDoublePushCount);
}


uint16_t enc2mdb_jogger_getMoveLeftFlag(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_MoveLeftFlag;
}


void enc2mdb_jogger_setMoveLeftFlag()
{
	g_MoveLeftFlag = true;
}


void enc2mdb_jogger_resetMoveLeftFlag()
{
	g_MoveLeftFlag = false;
}


uint16_t enc2mdb_jogger_getMoveRightFlag(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_MoveRightFlag;
}


void enc2mdb_jogger_setMoveRightFlag()
{
	g_MoveRightFlag = true;
}


void enc2mdb_jogger_resetMoveRightFlag()
{
	g_MoveRightFlag = false;
}


uint16_t enc2mdb_jogger_getMoveUpFlag(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_MoveUpFlag;
}


void enc2mdb_jogger_setMoveUpFlag()
{
	g_MoveUpFlag = true;
}


void enc2mdb_jogger_resetMoveUpFlag()
{
	g_MoveUpFlag = false;
}


uint16_t enc2mdb_jogger_getMoveDownFlag(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_MoveDownFlag;
}


void enc2mdb_jogger_setMoveDownFlag()
{
	g_MoveDownFlag = true;
}


void enc2mdb_jogger_resetMoveDownFlag()
{
	g_MoveDownFlag = false;
}


uint16_t enc2mdb_jogger_getMoveUpValue(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_MoveUpValue;
}


void enc2mdb_jogger_setMoveUpValue(uint16_t moveValue)
{
	if(moveValue > MAX_MOVE_VALUE)
		moveValue = MAX_MOVE_VALUE;
	g_MoveUpValue = moveValue;
}


uint16_t enc2mdb_jogger_getMoveDownValue(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_MoveDownValue;
}


void enc2mdb_jogger_setMoveDownValue(uint16_t moveValue)
{
	if(moveValue > MAX_MOVE_VALUE)
		moveValue = MAX_MOVE_VALUE;
	g_MoveDownValue = moveValue;
}


uint16_t enc2mdb_jogger_getMoveLeftValue(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_MoveLeftValue;
}


void enc2mdb_jogger_setMoveLeftValue(uint16_t moveValue)
{
	if(moveValue > MAX_MOVE_VALUE)
		moveValue = MAX_MOVE_VALUE;
	g_MoveLeftValue = moveValue;
}


uint16_t enc2mdb_jogger_getMoveRightValue(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_MoveRightValue;
}


void enc2mdb_jogger_setMoveRightValue(uint16_t moveValue)
{
	if(moveValue > MAX_MOVE_VALUE)
		moveValue = MAX_MOVE_VALUE;
	g_MoveRightValue = moveValue;
}


uint16_t enc2mdb_jogger_getFwMajor(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_FwMajor;
}


uint16_t enc2mdb_jogger_getFwMinor(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_FwMinor;
}


uint16_t enc2mdb_jogger_getFwPatch(uint16_t unusedPar)
{
	(void) unusedPar;
	return g_FwPatch;
}
