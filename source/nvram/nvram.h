/*
 * nvram.h
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */


#ifndef NVRAM_NVRAM_H_
#define NVRAM_NVRAM_H_


#define NVRAM_SIZE_BYTES			2048
#define UINT32_LEN 			 		4
#define TOTAL_NVRAM_BYTES 			(NVRAM_SIZE_BYTES + (UINT32_LEN*2))

//! Structure type containing variables to store in NVRAM using a specific
//! memory map.
typedef struct nvramData_tag {
	uint32_t marker;
	uint32_t lTotalLengthFile;
	uint8_t nvram_data[NVRAM_SIZE_BYTES];
}nvramData_t;


extern void		NVR_init(nvramData_t* pNvramData);
extern uint32_t	NVR_getTotalLenghFile(nvramData_t* pNvramData);
extern void		NVR_setTotalLenghFile(nvramData_t* pNvramData, uint32_t lenghtFile);
extern void		NVR_getData(nvramData_t* pNvramData, uint32_t index, uint32_t size, uint8_t * buffer);
extern void		NVR_setData(nvramData_t* pNvramData, uint32_t index, uint32_t size, uint8_t * buffer);
extern void		NVR_setPartialToEndData(nvramData_t* pNvramData, uint32_t index, uint32_t size, uint8_t * buffer);


#endif /* NVRAM_NVRAM_H_ */
