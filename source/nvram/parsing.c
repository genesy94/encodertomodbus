/*
 * parsing.c
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */



#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "parsing.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define abs(x)		(x>=0?x:-x)


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
/* global variable containing Config */
unsigned char IpAddress[4];
unsigned char MacAddress[6];
unsigned char ManagerIpAddress[4];
char fieldconfig[4][80];


/*******************************************************************************
 * Code
 ******************************************************************************/
/*
 * strtok_r redefinition: strtok_r isn't defined using Redlib
 *
 * public domain strtok_r() by Charlie Gordon
 *
 *   from comp.lang.c  9/14/2007
 *
 *      http://groups.google.com/group/comp.lang.c/msg/2ab1ecbb86646684
 *
 *     (Declaration that it's public domain):
 *      http://groups.google.com/group/comp.lang.c/msg/7c7b39328fefab9c
 */

static char* strtok_r(char *str, const char *delim, char **nextp) {
	char *ret;

	if (str == NULL) {
		str = *nextp;
	}

	str += strspn(str, delim);

	if (*str == '\0') {
		return NULL;
	}

	ret = str;

	str += strcspn(str, delim);

	if (*str) {
		*str++ = '\0';
	}

	*nextp = str;

	return ret;
}


/*-----------------------------------------------------------*/

/*! \brief parse the file containing the configuration
 *
 *  \param ptr		pointer to configuration file
 *
 */
void parsefile(char* ptr){
	const char del[] = "\n";
	const char del2[] = "= ";
	char* p;
	char* r;
	char* saved = NULL;
	int i = 0;
	for(p=strtok_r(ptr,del,&saved); p!=0; p=strtok_r(0,del,&saved)){
		r = p;
		while(*r!='=') r++;
		r = strtok(r,del2);
		strcpy(fieldconfig[i],r);
		i++;
	}
	return;
}


/*-----------------------------------------------------------*/

/*! \brief parse the field containing MAC or IP
 *
 *  \param par		type of parsed field
 *
 */
void parsestring(param par){
	short NbDigits = 0;
	const char del[] = ".:";
	char* p;
	unsigned int AddrByte;

	for(p=strtok(fieldconfig[par],del); p!=0; p=strtok(0,del)){
		if(par == IP){
			sscanf(p, "%03d", &AddrByte);
			IpAddress[NbDigits] = (unsigned char)AddrByte;
		}
		else if(par == MANAGER){
			sscanf(p, "%03d", &AddrByte);
			ManagerIpAddress[NbDigits] = (unsigned char)AddrByte;
		}
		else{
			sscanf(p, "%02x", &AddrByte);
			MacAddress[NbDigits] = (unsigned char)AddrByte;
		}
		NbDigits++;
	}
	return;
}


/*-----------------------------------------------------------*/

/*! \brief delete first line from a string
 *
 *  \param par		pointer to a string
 *
 */
char* deleteFirstLine(char* ptr){
	char *ret;
	const char del[] = "\n";
	strtok_r(ptr,del,&ret);
	return ret;
}

