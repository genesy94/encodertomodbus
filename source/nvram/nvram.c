/*
 * nvram.c
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */


#include <stdint.h>
#include <fsl_common.h>
#include <fsl_flash.h>
#include "parsing.h"
#include "nvram.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#ifndef SECTOR_INDEX_FROM_END
	#define SECTOR_INDEX_FROM_END 1U
#endif

/* Marker of config file present */
#define MARKER_TEMPLATE 	0xCAFEBABF

/* Number of flash's sectors protected */
#define PROTECTION_VALUE 	0xC0000000

#define APPVERSION_STRING_SIZE 		59
#define CONFIGLINES_STRING_SIZE 	84

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
/* Default config file */
/* Default config file */
const char appVersion[] = "AppVersion = Multicontrol_EncoderToModbus - Release 2.0.5\r\n";
const char configLines[] = "LocalIp = 192.168.21.1\r\nMacAddress = 00:04:25:1C:C1:01\r\nManagerIp = 192.168.21.210\r\n";

status_t result;    /* Return code from each flash driver function */

/*! @brief Flash driver Structure */
static flash_config_t s_flashDriver;

static pflash_prot_status_t flashProt;

static uint32_t markerAddress; /* Address of the target location */
static uint32_t lTotalLengthFileAddress; /* Address of the target location */
static uint32_t nvramDataAddress; /* Address of the target location */

static uint32_t failAddr, failDat;

static uint32_t pflashBlockBase = 0;
static uint32_t pflashTotalSize = 0;
static uint32_t pflashSectorSize = 0;


/*******************************************************************************
 * Code
 ******************************************************************************/
static void NVR_WriteAllData(nvramData_t* pNvramData) {

	/* erase flash */
	result = FLASH_Erase(&s_flashDriver, markerAddress, pflashSectorSize, kFLASH_ApiEraseKey);
	assert(kStatus_FLASH_Success == result);

	/* Verify sector if it's been erased. */
	result = FLASH_VerifyErase(&s_flashDriver, markerAddress, pflashSectorSize, kFTFx_MarginValueUser);
	assert(kStatus_FLASH_Success == result);

	/* write flash */
	result = FLASH_Program(&s_flashDriver, markerAddress, (uint8_t*)pNvramData, TOTAL_NVRAM_BYTES);
	assert(kStatus_FLASH_Success == result);

	/* Verify programming by Program Check command with user margin levels */
	result = FLASH_VerifyProgram(&s_flashDriver, markerAddress, TOTAL_NVRAM_BYTES, (uint8_t*)pNvramData, kFTFx_MarginValueUser,
								 &failAddr, &failDat);
	assert(kStatus_FLASH_Success == result);

	return;
}


static void NVR_ReadAllData(nvramData_t* pNvramData) {
    /* lettura flash */
	pNvramData->marker = *(volatile uint32_t *)(markerAddress);
	pNvramData->lTotalLengthFile = *(volatile uint32_t *)(lTotalLengthFileAddress);
	if(pNvramData->lTotalLengthFile < 0 || pNvramData->lTotalLengthFile > NVRAM_SIZE_BYTES)
		pNvramData->lTotalLengthFile = NVRAM_SIZE_BYTES;
    for (uint32_t i = 0; i < pNvramData->lTotalLengthFile; i++)
    {
    	pNvramData->nvram_data[i] = *(volatile uint8_t *)(nvramDataAddress + i);
    }

    return;
}



/*-----------------------------------------------------------*/

/*! \brief get the length of the config file in flash
 *
 *  \param pNvramData	pointer to flash data section
 *	\return pNvramData->lTotalLengthFile
 *
 */

uint32_t NVR_getTotalLenghFile(nvramData_t* pNvramData) {
	return pNvramData->lTotalLengthFile;
}

/*-----------------------------------------------------------*/

/*! \brief set the length of the config file
 *
 *  \param pNvramData	pointer to flash data section
 *  \param lengthFile	size of file
 *
 */

void NVR_setTotalLenghFile(nvramData_t* pNvramData, uint32_t lenghtFile) {

	pNvramData->lTotalLengthFile = lenghtFile;

	NVR_WriteAllData(pNvramData);

	NVR_ReadAllData(pNvramData);

	return;
}

/*-----------------------------------------------------------*/

/*! \brief read data in flash
 *
 *  \param pNvramData	pointer to flash data section
 *  \param index		first location in flash buffer
 *  \param size			dimension of file
 *  \param buffer		pointer to ram buffer
 *
 */

void NVR_getData(nvramData_t* pNvramData, uint32_t index, uint32_t size, uint8_t * buffer){
    for (uint32_t i = 0; i < size; i++)
    {
    	*(buffer + i) = pNvramData->nvram_data[index + i];
    }
    return;
}

/*-----------------------------------------------------------*/

/*! \brief write in flash data section
 *
 *  \param pNvramData	pointer to flash data section
 *  \param index		first location in flash buffer
 *  \param size			dimension of file
 *  \param buffer		pointer to ram buffer
 *
 *
 */

void NVR_setData(nvramData_t* pNvramData, uint32_t index, uint32_t size, uint8_t * buffer){

	//non sovrascrive la versione
	if(index == 0){
		buffer = (uint8_t*)deleteFirstLine((char*)buffer);
		index = APPVERSION_STRING_SIZE;
		size = strlen((char*)buffer);
	}

    /*rileggo dalla flash */
    for (uint32_t i = index; i < (index+size); i++)
    {
    	pNvramData->nvram_data[i] = buffer[i - APPVERSION_STRING_SIZE];
    }


	NVR_WriteAllData(pNvramData);

	NVR_ReadAllData(pNvramData);

	return;
}

/*-----------------------------------------------------------*/

/*! \brief write in flash partial to end data section
 *
 *  \param pNvramData	pointer to flash data section
 *  \param index		starting location in flash buffer
 *  \param size			dimension of data to write
 *  \param buffer		pointer to ram buffer
 *
 *
 */

void NVR_setPartialToEndData(nvramData_t* pNvramData, uint32_t index, uint32_t size, uint8_t * buffer) {

    /*rileggo dalla flash */
    for (uint32_t i = index; i < (index+size); i++)
    {
    	pNvramData->nvram_data[i] = buffer[i - index];
    }


	NVR_WriteAllData(pNvramData);

	NVR_ReadAllData(pNvramData);

	return;
}

/*-----------------------------------------------------------*/

/*! \brief inizialaize the flash memory
 *
 *  \param pNvramData	pointer to flash data section
 *
 */

void NVR_init(nvramData_t* pNvramData) {

	/* Clean up Flash driver Structure*/
	memset(&s_flashDriver, 0, sizeof(flash_config_t));
    /* Setup flash driver structure for device and initialize variables. */
    result = FLASH_Init(&s_flashDriver);

    assert(kStatus_FLASH_Success == result);

    /* Protezione flash da scrittura */
	FLASH_PflashGetProtection(&s_flashDriver, &flashProt);
	assert(kStatus_FLASH_Success == result);
	if(flashProt.protl != PROTECTION_VALUE){
	    flashProt.protl = PROTECTION_VALUE; //proteggo tutta la flash tranne gli ultimi due settori
	    FLASH_PflashSetProtection(&s_flashDriver, &flashProt);
	    assert(kStatus_FLASH_Success == result);
	}

	/* Get flash properties*/
	FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflash0BlockBaseAddr,
			&pflashBlockBase);
	FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflash0TotalSize,
			&pflashTotalSize);
	FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflash0SectorSize,
			&pflashSectorSize);

	/* Dest addresses */
	#if defined(FSL_FEATURE_FLASH_HAS_PFLASH_BLOCK_SWAP) && FSL_FEATURE_FLASH_HAS_PFLASH_BLOCK_SWAP
		/* Note: we should make sure that the sector shouldn't be swap indicator sector*/
		markerAddress = pflashBlockBase + (pflashTotalSize - (pflashSectorSize * 2));
	#else

		markerAddress = pflashBlockBase + (pflashTotalSize - pflashSectorSize);
	#endif
	lTotalLengthFileAddress = markerAddress + sizeof(uint32_t);
	nvramDataAddress = lTotalLengthFileAddress + sizeof(uint32_t);

	NVR_ReadAllData(pNvramData);

	/* inizializzazione flash con default config */
	if(pNvramData->marker != MARKER_TEMPLATE){
		char defaulConfig[APPVERSION_STRING_SIZE + CONFIGLINES_STRING_SIZE];
		strcpy(defaulConfig, appVersion);
		strcat(defaulConfig, configLines);
		pNvramData->marker = MARKER_TEMPLATE;
		pNvramData->lTotalLengthFile = sizeof(defaulConfig);
		memset(&pNvramData->nvram_data, 0, NVRAM_SIZE_BYTES);
		memcpy((void*)(pNvramData->nvram_data), (void*)defaulConfig, sizeof(defaulConfig));
	    NVR_WriteAllData(pNvramData);
	}

	return;
}

