/*
 * parsing.h
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */

#ifndef NVRAM_PARSING_H_
#define NVRAM_PARSING_H_

typedef enum{TRUE, FALSE} boolean;
typedef enum{IP = 1, MAC = 2, MANAGER = 3} param; //0 = AppVersion

extern void parsefile(char* ptr);
extern void parsestring(param par);
extern char* deleteFirstLine(char* ptr);

#endif /* NVRAM_PARSING_H_ */
