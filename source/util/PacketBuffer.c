/*
 * PacketBuffer.c
 *
 *  Created on: 5 Oct 2021
 *      Author: dario
 */

#include <stdint.h>
#include <stdbool.h>
#include "enc2mdb_types.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "PacketBuffer.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define ENC_QUEUE_LEN	 	5
#define MDB_QUEUE_LEN	 	80
#define FULL_QUEUE_TICKS	5
#define EMPTY_QUEUE_TICKS	1


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static xQueueHandle g_XEncoderQueue;
static xQueueHandle g_XMdbEventQueue;


/*******************************************************************************
 * Code
 ******************************************************************************/
void PBUF_init(void) {
	g_XEncoderQueue = xQueueCreate(ENC_QUEUE_LEN, sizeof(ENC2MDB_ENCODER_VALUE));
	g_XMdbEventQueue = xQueueCreate(MDB_QUEUE_LEN, sizeof(enum MDB_EVENT));
	return;
}


void PBUF_addEncoderPacket(ENC2MDB_ENCODER_VALUE* msg) {
	xQueueSendFromISR(g_XEncoderQueue, (void*)msg, pdFALSE);
	return;
}


uint32_t PBUF_getEncoderPacket(ENC2MDB_ENCODER_VALUE* msg) {
	return xQueueReceive(g_XEncoderQueue, (void *)msg, EMPTY_QUEUE_TICKS);
}


void PBUF_addMdbEventPacket(enum MDB_EVENT* event) {
	xQueueSend(g_XMdbEventQueue, (void*)event, FULL_QUEUE_TICKS);
	return;
}


uint32_t PBUF_getMdbEventPacket(enum MDB_EVENT* event) {
	return xQueueReceive(g_XMdbEventQueue, (void*)event, EMPTY_QUEUE_TICKS);
}
