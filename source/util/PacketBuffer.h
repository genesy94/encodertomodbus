/*
 * PacketBuffer.h
 *
 *  Created on: 5 Oct 2021
 *      Author: dario
 */

#ifndef UTIL_PACKETBUFFER_H_
#define UTIL_PACKETBUFFER_H_

extern void PBUF_init(void);
extern void PBUF_addEncoderPacket(ENC2MDB_ENCODER_VALUE* msg);
extern uint32_t PBUF_getEncoderPacket(ENC2MDB_ENCODER_VALUE* msg);
extern void PBUF_addMdbEventPacket(enum MDB_EVENT* event);
extern uint32_t PBUF_getMdbEventPacket(enum MDB_EVENT* event);

#endif /* UTIL_PACKETBUFFER_H_ */
