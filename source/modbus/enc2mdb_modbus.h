/*
 * modbus_slave.h
 *
 *  Created on: 1 Sep 2021
 *      Author: dario
 */

#ifndef ENC2MDB_MODBUS_H_
#define ENC2MDB_MODBUS_H_

extern uint16_t enc2mdb_getOneModbusRequest(uint8_t *inputStream, uint16_t streamLength, uint8_t* request);
extern uint16_t enc2mdb_modbusSlaveParseRequest(const uint8_t *request, const uint16_t requestLength, uint8_t *response);
extern void enc2mdb_modbusSlaveInit();

#endif /* ENC2MDB_MODBUS_H_ */
