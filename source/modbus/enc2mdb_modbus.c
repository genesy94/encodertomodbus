
/*
 * modbus_slave.c
 *
 *  Created on: 1 Sep 2021
 *      Author: dario
 */
#define LIGHTMODBUS_FULL
#define LIGHTMODBUS_DEBUG
#define LIGHTMODBUS_IMPL
#include <lightmodbus/lightmodbus.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include "enc2mdb_types.h"
#include <jogger/enc2mdb_jogger.h>
#include <jogger/enc2mdb_jogger_neopixel.h>


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define MODBUS_OFFSET				29700U
#define NUM_OF_REGISTERS			200U
#define NUM_OF_COLOR_COMP			3U

enum REG_VALUE
{
	reg_Online = 10,
	reg_EncoderCounter = 12,
	reg_PushFlag = 60,
	reg_PushCounter,
	reg_DoublePushCounter,
	reg_MoveDownFlag,
	reg_MoveUpFlag,
	reg_MoveRightFlag,
	reg_MoveLeftFlag,
	reg_MoveDownValue,
	reg_MoveUpValue,
	reg_MoveRightValue,
	reg_MoveLeftValue,
	reg_FwVerMajor,
	reg_FwVerMinor,
	reg_FwVerPatch,
	reg_NeopixelColorRCmd = 80,
	reg_NeopixelColorGCmd,
	reg_NeopixelColorBCmd,
	reg_NeopixelModeCmd,
	reg_NeopixelSpeedCmd,
	reg_NeopixelColorDimmingCmd,
	reg_NeopixelCurrRotPixCmd,
	reg_NeopixelPixOn0Cmd,
	reg_NeopixelPixOn1Cmd,
	reg_NeopixelPixOn2Cmd,
	reg_NeopixelPixOn3Cmd,
	reg_NeopixelPixOn4Cmd,
	reg_NeopixelPixOn5Cmd,
	reg_NeopixelPixOn6Cmd,
	reg_NeopixelPixOn7Cmd,
	reg_NeopixelPixOn8Cmd,
	reg_NeopixelPixOn9Cmd,
	reg_NeopixelPixOn10Cmd,
	reg_NeopixelPixOn11Cmd,
	reg_NeopixelPixOn12Cmd,
	reg_NeopixelPixOn13Cmd,
	reg_NeopixelPixOn14Cmd,
	reg_NeopixelPixOn15Cmd,
	reg_NeopixelPixOn16Cmd,
	reg_NeopixelPixOn17Cmd,
	reg_NeopixelPixOn18Cmd,
	reg_NeopixelPixOn19Cmd,
	reg_NeopixelPixOn20Cmd,
	reg_NeopixelPixOn21Cmd,
	reg_NeopixelPixOn22Cmd,
	reg_NeopixelPixOn23Cmd,
	reg_NeopixelPixColorR0Cmd,
	reg_NeopixelPixColorG0Cmd,
	reg_NeopixelPixColorB0Cmd,
	reg_NeopixelPixColorR1Cmd,
	reg_NeopixelPixColorG1Cmd,
	reg_NeopixelPixColorB1Cmd,
	reg_NeopixelPixColorR2Cmd,
	reg_NeopixelPixColorG2Cmd,
	reg_NeopixelPixColorB2Cmd,
	reg_NeopixelPixColorR3Cmd,
	reg_NeopixelPixColorG3Cmd,
	reg_NeopixelPixColorB3Cmd,
	reg_NeopixelPixColorR4Cmd,
	reg_NeopixelPixColorG4Cmd,
	reg_NeopixelPixColorB4Cmd,
	reg_NeopixelPixColorR5Cmd,
	reg_NeopixelPixColorG5Cmd,
	reg_NeopixelPixColorB5Cmd,
	reg_NeopixelPixColorR6Cmd,
	reg_NeopixelPixColorG6Cmd,
	reg_NeopixelPixColorB6Cmd,
	reg_NeopixelPixColorR7Cmd,
	reg_NeopixelPixColorG7Cmd,
	reg_NeopixelPixColorB7Cmd,
	reg_NeopixelPixColorR8Cmd,
	reg_NeopixelPixColorG8Cmd,
	reg_NeopixelPixColorB8Cmd,
	reg_NeopixelPixColorR9Cmd,
	reg_NeopixelPixColorG9Cmd,
	reg_NeopixelPixColorB9Cmd,
	reg_NeopixelPixColorR10Cmd,
	reg_NeopixelPixColorG10Cmd,
	reg_NeopixelPixColorB10Cmd,
	reg_NeopixelPixColorR11Cmd,
	reg_NeopixelPixColorG11Cmd,
	reg_NeopixelPixColorB11Cmd,
	reg_NeopixelPixColorR12Cmd,
	reg_NeopixelPixColorG12Cmd,
	reg_NeopixelPixColorB12Cmd,
	reg_NeopixelPixColorR13Cmd,
	reg_NeopixelPixColorG13Cmd,
	reg_NeopixelPixColorB13Cmd,
	reg_NeopixelPixColorR14Cmd,
	reg_NeopixelPixColorG14Cmd,
	reg_NeopixelPixColorB14Cmd,
	reg_NeopixelPixColorR15Cmd,
	reg_NeopixelPixColorG15Cmd,
	reg_NeopixelPixColorB15Cmd,
	reg_NeopixelPixColorR16Cmd,
	reg_NeopixelPixColorG16Cmd,
	reg_NeopixelPixColorB16Cmd,
	reg_NeopixelPixColorR17Cmd,
	reg_NeopixelPixColorG17Cmd,
	reg_NeopixelPixColorB17Cmd,
	reg_NeopixelPixColorR18Cmd,
	reg_NeopixelPixColorG18Cmd,
	reg_NeopixelPixColorB18Cmd,
	reg_NeopixelPixColorR19Cmd,
	reg_NeopixelPixColorG19Cmd,
	reg_NeopixelPixColorB19Cmd,
	reg_NeopixelPixColorR20Cmd,
	reg_NeopixelPixColorG20Cmd,
	reg_NeopixelPixColorB20Cmd,
	reg_NeopixelPixColorR21Cmd,
	reg_NeopixelPixColorG21Cmd,
	reg_NeopixelPixColorB21Cmd,
	reg_NeopixelPixColorR22Cmd,
	reg_NeopixelPixColorG22Cmd,
	reg_NeopixelPixColorB22Cmd,
	reg_NeopixelPixColorR23Cmd,
	reg_NeopixelPixColorG23Cmd,
	reg_NeopixelPixColorB23Cmd,
	reg_NeopixelFwVerMajor,
	reg_NeopixelFwVerMinor,
	reg_NeopixelFwVerPatch
};


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static ModbusSlave g_ModbusSlave;
static ModbusErrorInfo g_ModbusError;
static ENC2MDB_READ_FEEDBACK_FUNC g_FeedbackRegisters[NUM_OF_REGISTERS];
static ENC2MDB_WRITE_COMMAND_FUNC g_CommandRegisters[NUM_OF_REGISTERS];


/*******************************************************************************
 * Code
 ******************************************************************************/
/*
	A register callback that prints out everything what's happening
*/
ModbusError registerCallback(
	const ModbusSlave *slave,
	const ModbusRegisterCallbackArgs *args,
	ModbusRegisterCallbackResult *result)
{
	ModbusError resultMdbErr = MODBUS_OK;
	uint16_t index = 0U, paramOffset = 0U;

	switch (args->query)
	{
		// Pretend we allow all access
		// Tip: Change MODBUS_EXCEP_NONE to something else
		// 		and see what happens
		case MODBUS_REGQ_R_CHECK:
		case MODBUS_REGQ_W_CHECK:
			result->exceptionCode = MODBUS_EXCEP_NONE;
			break;

		case MODBUS_REGQ_R:
			index = (uint16_t)(args->index - MODBUS_OFFSET);
			if(index >= NUM_OF_REGISTERS){
				result->exceptionCode = MODBUS_ERROR_RANGE;
				resultMdbErr = MODBUS_ERROR_RANGE;
				result->value = 0U;
			}
			else if(args->type != MODBUS_HOLDING_REGISTER) {
				result->exceptionCode = MODBUS_ERROR_FUNCTION;
				resultMdbErr = MODBUS_ERROR_FUNCTION;
				result->value = 0U;
			}
			else if(g_FeedbackRegisters[index] == NULL) {
				result->value = 0U;
			}
			else {
				if(index >= reg_NeopixelPixOn0Cmd && index <= reg_NeopixelPixOn23Cmd)
					paramOffset = index - reg_NeopixelPixOn0Cmd;
				else if(index >= reg_NeopixelPixColorR0Cmd && index <= reg_NeopixelPixColorB23Cmd)
					paramOffset = (uint16_t)((index - reg_NeopixelPixColorR0Cmd)/NUM_OF_COLOR_COMP);
				else
					paramOffset = index;
				result->value = (*g_FeedbackRegisters[index])(paramOffset);
			}
			break;

		case MODBUS_REGQ_W:
			index = (uint16_t)(args->index - MODBUS_OFFSET);
			result->value = 0U;
			if(index >= NUM_OF_REGISTERS){
				result->exceptionCode = MODBUS_ERROR_RANGE;
				resultMdbErr = MODBUS_ERROR_RANGE;
			}
			else if(args->type != MODBUS_HOLDING_REGISTER) {
				result->exceptionCode = MODBUS_ERROR_FUNCTION;
				resultMdbErr = MODBUS_ERROR_FUNCTION;
			}
			else if(g_CommandRegisters[index] != NULL){
				if(index >= reg_NeopixelPixOn0Cmd && index <= reg_NeopixelPixOn23Cmd)
					paramOffset = index - reg_NeopixelPixOn0Cmd;
				else if(index >= reg_NeopixelPixColorR0Cmd && index <= reg_NeopixelPixColorB23Cmd)
					paramOffset = (uint16_t)((index - reg_NeopixelPixColorR0Cmd)/NUM_OF_COLOR_COMP);
				else
					paramOffset = index;
				(*g_CommandRegisters[index])(args->value, paramOffset);
			}
			break;
	}

	return resultMdbErr;
}

/*
	Exception callback for printing out exceptions
*/
ModbusError slaveExceptionCallback(const ModbusSlave *slave, uint8_t function, ModbusExceptionCode code)
{
	// Always return MODBUS_OK
	return MODBUS_OK;
}

static void registersInit()
{
	uint8_t i;
	for(i = 0; i < NUM_OF_REGISTERS; ++i) {
		g_FeedbackRegisters[i] = &enc2mdb_jogger_getUnusedRegValue;
	}

	g_FeedbackRegisters[reg_Online] = &enc2mdb_jogger_getOnlineFlag;
	g_FeedbackRegisters[reg_EncoderCounter] = &enc2mdb_jogger_getRotationCounter;
	g_FeedbackRegisters[reg_PushFlag] = &enc2mdb_jogger_getPushFlag;
	g_FeedbackRegisters[reg_PushCounter] = &enc2mdb_jogger_getPushCounter;
	g_FeedbackRegisters[reg_DoublePushCounter] = &enc2mdb_jogger_getDoublePushCounter;
	g_FeedbackRegisters[reg_MoveUpFlag] = &enc2mdb_jogger_getMoveUpFlag;
	g_FeedbackRegisters[reg_MoveDownFlag] = &enc2mdb_jogger_getMoveDownFlag;
	g_FeedbackRegisters[reg_MoveLeftFlag] = &enc2mdb_jogger_getMoveLeftFlag;
	g_FeedbackRegisters[reg_MoveRightFlag] = &enc2mdb_jogger_getMoveRightFlag;
	g_FeedbackRegisters[reg_MoveUpValue] = &enc2mdb_jogger_getMoveUpValue;
	g_FeedbackRegisters[reg_MoveDownValue] = &enc2mdb_jogger_getMoveDownValue;
	g_FeedbackRegisters[reg_MoveLeftValue] = &enc2mdb_jogger_getMoveLeftValue;
	g_FeedbackRegisters[reg_MoveRightValue] = &enc2mdb_jogger_getMoveRightValue;
	g_FeedbackRegisters[reg_NeopixelColorRCmd] = &enc2mdb_jogger_getNeopixelRingColorR;
	g_FeedbackRegisters[reg_NeopixelColorGCmd] = &enc2mdb_jogger_getNeopixelRingColorG;
	g_FeedbackRegisters[reg_NeopixelColorBCmd] = &enc2mdb_jogger_getNeopixelRingColorB;
	g_FeedbackRegisters[reg_NeopixelModeCmd] = &enc2mdb_jogger_getNeopixelRingMode;
	g_FeedbackRegisters[reg_NeopixelSpeedCmd] = &enc2mdb_jogger_getNeopixelRingSpeed;
	g_FeedbackRegisters[reg_NeopixelColorDimmingCmd] = &enc2mdb_jogger_getNeopixelDimmingValue;
	g_FeedbackRegisters[reg_FwVerMajor] = &enc2mdb_jogger_getFwMajor;
	g_FeedbackRegisters[reg_FwVerMinor] = &enc2mdb_jogger_getFwMinor;
	g_FeedbackRegisters[reg_FwVerPatch] = &enc2mdb_jogger_getFwPatch;
	g_FeedbackRegisters[reg_NeopixelCurrRotPixCmd] = &enc2mdb_jogger_getNeopixelRotatingPixel;
	for(i = reg_NeopixelPixOn0Cmd; i <= reg_NeopixelPixOn23Cmd; ++i) {
		g_FeedbackRegisters[i] = &enc2mdb_jogger_getNeopixelSinglePixelOn;
	}
	for(i = reg_NeopixelPixColorR0Cmd; i <= reg_NeopixelPixColorB23Cmd; ++i) {
		if((i - reg_NeopixelPixColorR0Cmd) % NUM_OF_COLOR_COMP == 0)
			g_FeedbackRegisters[i] = &enc2mdb_jogger_getNeopixelSinglePixelColorR;
		else if((i - reg_NeopixelPixColorR0Cmd) % NUM_OF_COLOR_COMP == 1)
			g_FeedbackRegisters[i] = &enc2mdb_jogger_getNeopixelSinglePixelColorG;
		else
			g_FeedbackRegisters[i] = &enc2mdb_jogger_getNeopixelSinglePixelColorB;
	}
	g_FeedbackRegisters[reg_NeopixelFwVerMajor] = &enc2mdb_jogger_getNeopixelRingFwMajor;
	g_FeedbackRegisters[reg_NeopixelFwVerMinor] = &enc2mdb_jogger_getNeopixelRingFwMinor;
	g_FeedbackRegisters[reg_NeopixelFwVerPatch] = &enc2mdb_jogger_getNeopixelRingFwPatch;

	for(i = 0; i < NUM_OF_REGISTERS; ++i) {
		g_CommandRegisters[i] = &enc2mdb_jogger_setUnusedRegValue;
	}

	g_CommandRegisters[reg_NeopixelColorRCmd] = &enc2mdb_jogger_setNeopixelRingColorR;
	g_CommandRegisters[reg_NeopixelColorGCmd] = &enc2mdb_jogger_setNeopixelRingColorG;
	g_CommandRegisters[reg_NeopixelColorBCmd] = &enc2mdb_jogger_setNeopixelRingColorB;
	g_CommandRegisters[reg_NeopixelModeCmd] = &enc2mdb_jogger_setNeopixelRingMode;
	g_CommandRegisters[reg_NeopixelSpeedCmd] = &enc2mdb_jogger_setNeopixelRingSpeed;
	g_CommandRegisters[reg_NeopixelColorDimmingCmd] = &enc2mdb_jogger_setNeopixelDimmingValue;
	g_CommandRegisters[reg_NeopixelCurrRotPixCmd] = &enc2mdb_jogger_setNeopixelRotatingPixel;
	for(i = reg_NeopixelPixOn0Cmd; i <= reg_NeopixelPixOn23Cmd; ++i) {
		g_CommandRegisters[i] = &enc2mdb_jogger_setNeopixelSinglePixelOn;
	}
	for(i = reg_NeopixelPixColorR0Cmd; i <= reg_NeopixelPixColorB23Cmd; ++i) {
		if((i - reg_NeopixelPixColorR0Cmd) % NUM_OF_COLOR_COMP == 0)
			g_CommandRegisters[i] = &enc2mdb_jogger_setNeopixelSinglePixelColorR;
		else if((i - reg_NeopixelPixColorR0Cmd) % NUM_OF_COLOR_COMP == 1)
			g_CommandRegisters[i] = &enc2mdb_jogger_setNeopixelSinglePixelColorG;
		else
			g_CommandRegisters[i] = &enc2mdb_jogger_setNeopixelSinglePixelColorB;
	}
}

uint16_t enc2mdb_getOneModbusRequest(uint8_t *inputStream, uint16_t streamLength, uint8_t* request)
{
	uint16_t firstModbusMessageLength = 0;

	if (streamLength >= MODBUS_TCP_ADU_MIN && streamLength <= MODBUS_TCP_ADU_MAX) {
		firstModbusMessageLength = modbusRBE(&inputStream[4]) + 6;
		memcpy(request, inputStream, firstModbusMessageLength);
		memcpy(inputStream, inputStream + firstModbusMessageLength, streamLength - firstModbusMessageLength);
	}

	return firstModbusMessageLength;
}

uint16_t enc2mdb_modbusSlaveParseRequest(const uint8_t *request, const uint16_t requestLength, uint8_t *response)
{
	uint16_t responseLength = 0U;
	g_ModbusError = modbusParseRequestTCP(&g_ModbusSlave, request, requestLength);
	if (modbusIsOk(g_ModbusError)) {
		responseLength = modbusSlaveGetResponseLength(&g_ModbusSlave);
		memcpy(response, modbusSlaveGetResponse(&g_ModbusSlave), responseLength);
	}
	// Check for any serious errors
	assert(modbusGetGeneralError(g_ModbusError) == MODBUS_OK && "slave error!");
	return responseLength;
}

void enc2mdb_modbusSlaveInit()
{
	registersInit();
	// Init slave
	g_ModbusError = modbusSlaveInit(
		&g_ModbusSlave,
		registerCallback,
		slaveExceptionCallback,
		modbusDefaultAllocator,
		modbusSlaveDefaultFunctions,
		modbusSlaveDefaultFunctionCount);
	assert(modbusIsOk(g_ModbusError) && "modbusSlaveInit() failed!");
}



