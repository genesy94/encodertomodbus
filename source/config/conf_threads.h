/*
 * conf_threads.h
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */

#ifndef CONFIG_CONF_THREADS_H_
#define CONFIG_CONF_THREADS_H_


// stack size
#define MAIN_STACK_SIZE			  			2048

#define TCP_SLAVE_STACK_SIZE			  	1024

#define BASIC_TFTP_SERVER_STACK_SIZE      	512


// task size
#define MAIN_TASK_PRIORITY			  		(DEFAULT_THREAD_PRIO)

#define TCP_SLAVE_TASK_PRIORITY		  		(DEFAULT_THREAD_PRIO)

#define BASIC_TFTP_SERVER_TASK_PRIORITY   	(DEFAULT_THREAD_PRIO)


/*! Number of threads that can be started with sys_thread_new() */
#define SYS_THREAD_MAX                    	6


#endif /* CONFIG_CONF_THREADS_H_ */

