/*
 * enc2mdb.c
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */

#include <stdint.h>
#include <stdbool.h>
#include "enc2mdb_types.h"
#include "enc2mdb_adc.h"
#include "enc2mdb_button.h"
#include "enc2mdb_rot.h"
#include "enc2mdb_jogger.h"
#include "enc2mdb_jogger_neopixel.h"
#include "enc2mdb_neopixring_uart.h"
#include "PacketBuffer.h"
#include "FreeRTOS.h"
#include "timers.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define RING_FW_TIMER_PERIOD_MS 	(3000 / portTICK_PERIOD_MS)
#define ROT_PIXEL_TIMER_PERIOD_MS 	(400 / portTICK_PERIOD_MS)
#define EXIT_WAIT_TIMER_PERIOD_MS 	200U
#define WAIT_BLINK_PERIOD_MS 		200U
#define UNUSED_PARAM				0U
#define ROTATE_OFFSET				6U


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static TimerHandle_t g_RotatingPixelTimerHandle = NULL;
static TimerHandle_t g_ExitWaitTimerHandle = NULL;
static TimerHandle_t g_WaitBlinkTimerHandle = NULL;
static enum JOGGER_EVENT g_LastEventSentToRing;
static enum MOUNT_ROTATION g_MountRotation;
static uint8_t g_StartingPixel = 0U;
static bool g_SinglePixEvenColArrived = false;
static bool g_SinglePixOddColArrived = false;
static bool g_SinglePixOnOffColArrived = false;
static enum MDB_EVENT g_NextEventAfterWait = noEvent;
static enum RUN_STATE g_RunState = sIdle;
static enum NEOPIXEL_RING_MODE g_ModeAtWaitTime;


/*******************************************************************************
 * Code
 ******************************************************************************/
static void changeStateAfterTimeout() {
	if(g_SinglePixEvenColArrived == true)
		g_RunState = sSinglePixEvenColUpd;
	else if(g_SinglePixOddColArrived == true)
		g_RunState = sSinglePixOddColUpd;
	else if(g_SinglePixOnOffColArrived == true)
		g_RunState = sSinglePixelOnOffUpd;
	else
		g_RunState = sManageWaitingEvent;
}


static void rotatingPixelTimerCallback(TimerHandle_t xTimer)
{
	if(g_LastEventSentToRing == evRot) {
		enc2mdb_neopixring_uart_send_jogger_event(evNone);
		g_LastEventSentToRing = evNone;
	}
}


static void exitFromWaitTimerCallback(TimerHandle_t xTimer)
{
	changeStateAfterTimeout();
}


static uint8_t translateRotPix(uint8_t rotPixInput)
{
	if(g_MountRotation == cwise)
		return (uint8_t)((rotPixInput + enc2mdb_jogger_getNeopixelNumOfPixels() - ROTATE_OFFSET) % enc2mdb_jogger_getNeopixelNumOfPixels());
	if(g_MountRotation == ccwise)
		return (rotPixInput + ROTATE_OFFSET) % enc2mdb_jogger_getNeopixelNumOfPixels();
	return rotPixInput;
}


static void setNeopixelRingMoveEvent(enum JOGGER_EVENT eventButton, enum JOGGER_EVENT eventAdc, enum JOGGER_EVENT eventRot, uint8_t rotatingPixel)
{
	/* button event management */
	if(eventButton != evNone) {
		if(g_LastEventSentToRing != evPush) {
			enc2mdb_neopixring_uart_send_jogger_event(eventButton);
			if(g_LastEventSentToRing == evRot)
				xTimerStop(g_RotatingPixelTimerHandle, 0);
			g_LastEventSentToRing = evPush;
		}
	}
	/* jogger event management */
	else if((eventAdc != evNone) ) {
		enc2mdb_neopixring_uart_send_jogger_event(eventAdc);
		g_LastEventSentToRing = eventAdc;
	}
	/* encoder event management */
	else if((eventRot != evNone) && ((g_LastEventSentToRing == evNone) || (g_LastEventSentToRing == evRot))) {
		enc2mdb_neopixring_uart_send_encoder_event(evRot, translateRotPix(rotatingPixel));
		enc2mdb_jogger_setNeopixelRotatingPixel((uint16_t) rotatingPixel, UNUSED_PARAM);
		if(g_LastEventSentToRing == evRot)
			xTimerStop(g_RotatingPixelTimerHandle, 0);
		g_LastEventSentToRing = evRot;
		xTimerStart(g_RotatingPixelTimerHandle, 0);
	}
	/* no event management */
	else if(((g_LastEventSentToRing != evNone) && (g_LastEventSentToRing != evRot) && (g_LastEventSentToRing != evPush)) || ((g_LastEventSentToRing == evPush) && (eventButton == evNone))) {
		enc2mdb_neopixring_uart_send_jogger_event(evNone);
		g_LastEventSentToRing = evNone;
	}
}


static void manageEvenPixelColorChange()
{
	uint8_t i, j=0U;
	uint8_t pixelsOnOff[enc2mdb_jogger_getNeopixelNumOfPixels()];
	ENC2MDB_RGB_VALUE pixelsColorToUpdate[enc2mdb_jogger_getNeopixelNumOfPixels()];

	for(i = g_StartingPixel; i < g_StartingPixel + enc2mdb_jogger_getNeopixelNumOfPixels(); ++i) {
		pixelsColorToUpdate[i % enc2mdb_jogger_getNeopixelNumOfPixels()].colorR = enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorR(j);
		pixelsColorToUpdate[i % enc2mdb_jogger_getNeopixelNumOfPixels()].colorG = enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorG(j);
		pixelsColorToUpdate[i % enc2mdb_jogger_getNeopixelNumOfPixels()].colorB = enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorB(j);
		++j;
	}
	enc2mdb_neopixring_uart_set_all_even_pixel_color(pixelsColorToUpdate, enc2mdb_jogger_getNeopixelNumOfPixels());
}


static void manageOddPixelColorChange()
{
	uint8_t i, j=0U;
	uint8_t pixelsOnOff[enc2mdb_jogger_getNeopixelNumOfPixels()];
	ENC2MDB_RGB_VALUE pixelsColorToUpdate[enc2mdb_jogger_getNeopixelNumOfPixels()];

	for(i = g_StartingPixel; i < g_StartingPixel + enc2mdb_jogger_getNeopixelNumOfPixels(); ++i) {
		pixelsColorToUpdate[i % enc2mdb_jogger_getNeopixelNumOfPixels()].colorR = enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorR(j);
		pixelsColorToUpdate[i % enc2mdb_jogger_getNeopixelNumOfPixels()].colorG = enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorG(j);
		pixelsColorToUpdate[i % enc2mdb_jogger_getNeopixelNumOfPixels()].colorB = enc2mdb_jogger_getNeopixelEffectiveSinglePixelColorB(j);
		++j;
	}
	enc2mdb_neopixring_uart_set_all_odd_pixel_color(pixelsColorToUpdate, enc2mdb_jogger_getNeopixelNumOfPixels());
}


static void managePixelOnOff()
{
	uint8_t i, j=0U;
	uint8_t pixelsOnOff[enc2mdb_jogger_getNeopixelNumOfPixels()];
	ENC2MDB_RGB_VALUE pixelsColorToUpdate[enc2mdb_jogger_getNeopixelNumOfPixels()];

	for(i = g_StartingPixel; i < g_StartingPixel + enc2mdb_jogger_getNeopixelNumOfPixels(); ++i) {
		pixelsOnOff[i % enc2mdb_jogger_getNeopixelNumOfPixels()] = enc2mdb_jogger_getNeopixelSinglePixelOn(j);
		++j;
	}
	enc2mdb_neopixring_uart_set_all_pixel_on_off(pixelsOnOff, enc2mdb_jogger_getNeopixelNumOfPixels());
}


static void checkJoggerMovements()
{
	enum JOGGER_EVENT eventButton, eventRot, eventAdc;
	enum NEOPIXEL_RING_MODE currentMode;
	uint8_t rotatingPixel;

	enc2mdb_button_run(&eventButton);
	enc2mdb_rot_run(&eventRot, &rotatingPixel);
	enc2mdb_adc_run(&eventAdc);
	currentMode = (enum NEOPIXEL_RING_MODE)enc2mdb_jogger_getNeopixelRingMode(UNUSED_PARAM);
	if(currentMode == modeFollowMoveOneColor || currentMode == modeFollowMoveColorPerPixel)
		setNeopixelRingMoveEvent(eventButton, eventAdc, eventRot, rotatingPixel);
}


static void idleState(enum MDB_EVENT event)
{
	switch(event) {
		case jogMode:
			enc2mdb_neopixring_uart_set_mode((uint8_t)enc2mdb_jogger_getNeopixelRingMode(UNUSED_PARAM), (uint8_t)enc2mdb_jogger_getNeopixelRingSpeed(UNUSED_PARAM));
			break;
		case jogSpeed:
			enc2mdb_neopixring_uart_set_speed((uint8_t)enc2mdb_jogger_getNeopixelRingSpeed(UNUSED_PARAM));
			break;
		case generalColor:
			enc2mdb_neopixring_uart_set_color((uint8_t)enc2mdb_jogger_getNeopixelRingEffectiveColorR(UNUSED_PARAM),
					(uint8_t)enc2mdb_jogger_getNeopixelRingEffectiveColorG(UNUSED_PARAM),
					(uint8_t)enc2mdb_jogger_getNeopixelRingEffectiveColorB(UNUSED_PARAM));
			break;
		case singleEvenPixelColor:
			g_SinglePixEvenColArrived = true;
			xTimerStart(g_ExitWaitTimerHandle, 0);
			g_ModeAtWaitTime = (enum NEOPIXEL_RING_MODE)enc2mdb_jogger_getNeopixelRingMode(UNUSED_PARAM);
			g_RunState = sSinglePixWait;
			break;
		case singleOddPixelColor:
			g_SinglePixOddColArrived = true;
			xTimerStart(g_ExitWaitTimerHandle, 0);
			g_ModeAtWaitTime = (enum NEOPIXEL_RING_MODE)enc2mdb_jogger_getNeopixelRingMode(UNUSED_PARAM);
			g_RunState = sSinglePixWait;
			break;
		case singlePixelOnOff:
			g_SinglePixOnOffColArrived = true;
			xTimerStart(g_ExitWaitTimerHandle, 0);
			g_ModeAtWaitTime = (enum NEOPIXEL_RING_MODE)enc2mdb_jogger_getNeopixelRingMode(UNUSED_PARAM);
			g_RunState = sSinglePixWait;
			break;
		default:
			break;
	}
}


static void singlePixWaitState(enum MDB_EVENT event)
{
	bool exitFromWait = false;
	enum NEOPIXEL_RING_MODE currentMode;

	switch(event) {
		case jogMode:
			g_NextEventAfterWait = jogMode;
			exitFromWait = true;
			break;
		case jogSpeed:
			g_NextEventAfterWait = jogSpeed;
			exitFromWait = true;
			break;
		case generalColor:
			g_NextEventAfterWait = generalColor;
			exitFromWait = true;
			break;
		case singleEvenPixelColor:
			xTimerReset(g_ExitWaitTimerHandle, 0);
			g_SinglePixEvenColArrived = true;
			break;
		case singleOddPixelColor:
			xTimerReset(g_ExitWaitTimerHandle, 0);
			g_SinglePixOddColArrived = true;
			break;
		case singlePixelOnOff:
			xTimerReset(g_ExitWaitTimerHandle, 0);
			g_SinglePixOnOffColArrived = true;
			break;
		default:
			break;
	}
	if(exitFromWait == true) {
		xTimerStop(g_ExitWaitTimerHandle, 0);
		//se cambio modalità mentre ero in flashing, dopo fine massimo ciclo blink spegni tutto, poi riprendi la gestione
		if((g_ModeAtWaitTime == modeFlashingOneColor || g_ModeAtWaitTime == modeFlashingColorPerPixel) && event == jogMode) {
			enc2mdb_neopixring_uart_set_mode(modeOff, (uint8_t)enc2mdb_jogger_getNeopixelRingSpeed(UNUSED_PARAM));
			xTimerStart(g_WaitBlinkTimerHandle, 0);
			g_RunState = sWaitingTimer;
		}
		else {
			if(g_SinglePixEvenColArrived == true)
				g_RunState = sSinglePixEvenColUpd;
			else if(g_SinglePixOddColArrived == true)
				g_RunState = sSinglePixOddColUpd;
			else
				g_RunState = sSinglePixelOnOffUpd;
		}
	}
}


static void manageWaitingEvent()
{
	switch(g_NextEventAfterWait) {
		case jogMode:
			enc2mdb_neopixring_uart_set_mode((uint8_t)enc2mdb_jogger_getNeopixelRingMode(UNUSED_PARAM), (uint8_t)enc2mdb_jogger_getNeopixelRingSpeed(UNUSED_PARAM));
			break;
		case jogSpeed:
			enc2mdb_neopixring_uart_set_speed((uint8_t)enc2mdb_jogger_getNeopixelRingSpeed(UNUSED_PARAM));
			break;
		case generalColor:
			enc2mdb_neopixring_uart_set_color((uint8_t)enc2mdb_jogger_getNeopixelRingEffectiveColorR(UNUSED_PARAM),
					(uint8_t)enc2mdb_jogger_getNeopixelRingEffectiveColorG(UNUSED_PARAM),
					(uint8_t)enc2mdb_jogger_getNeopixelRingEffectiveColorB(UNUSED_PARAM));
			break;
		default:
			break;
	}
	g_NextEventAfterWait = noEvent;
	g_RunState = sIdle;
}


static void singlePixEvenColUpdState()
{
	manageEvenPixelColorChange();
	g_SinglePixEvenColArrived = false;
	if((g_ModeAtWaitTime == modeFlashingOneColor || g_ModeAtWaitTime == modeFlashingColorPerPixel) && g_NextEventAfterWait != jogMode) {
		xTimerStart(g_WaitBlinkTimerHandle, 0);
		g_RunState = sWaitingTimer;
	}
	else
		changeStateAfterTimeout();
}


static void singlePixOddColUpdState()
{
	manageOddPixelColorChange();
	g_SinglePixOddColArrived = false;
	if((g_ModeAtWaitTime == modeFlashingOneColor || g_ModeAtWaitTime == modeFlashingColorPerPixel) && g_NextEventAfterWait != jogMode) {
		xTimerStart(g_WaitBlinkTimerHandle, 0);
		g_RunState = sWaitingTimer;
	}
	else
		changeStateAfterTimeout();
}


static void singlePixOnOffState()
{
	managePixelOnOff();
	g_SinglePixOnOffColArrived = false;
	if((g_ModeAtWaitTime == modeFlashingOneColor || g_ModeAtWaitTime == modeFlashingColorPerPixel) && g_NextEventAfterWait != jogMode) {
		xTimerStart(g_WaitBlinkTimerHandle, 0);
		g_RunState = sWaitingTimer;
	}
	else
		changeStateAfterTimeout();
}


void enc2mdb_run(ENC2MDB_MODBUSSLAVE* self)
{
	uint8_t major, minor, patch;
	enum MDB_EVENT event = noEvent;

	if(enc2mdb_jogger_getOnlineFlag(UNUSED_PARAM) == false) {
		if(enc2mdb_neopixring_uart_is_fw_ver_resp_arrived()) {
			enc2mdb_neopixring_uart_get_fw_ver(&major, &minor, &patch);
			enc2mdb_jogger_setNeopixelRingFwMajor((uint16_t)major);
			enc2mdb_jogger_setNeopixelRingFwMinor((uint16_t)minor);
			enc2mdb_jogger_setNeopixelRingFwPatch((uint16_t)patch);
			enc2mdb_jogger_setOnlineFlag();
		}
		else
			return;
	}

	PBUF_getMdbEventPacket(&event);
	switch(g_RunState) {
		case sIdle:
			if(event != noEvent)
				idleState(event);
			break;
		case sSinglePixWait:
			if(event != noEvent)
				singlePixWaitState(event);
			break;
		case sSinglePixEvenColUpd:
			singlePixEvenColUpdState();
			break;
		case sSinglePixOddColUpd:
			singlePixOddColUpdState();
			break;
		case sSinglePixelOnOffUpd:
			singlePixOnOffState();
			break;
		case sManageWaitingEvent:
			manageWaitingEvent();
			break;
		default:
			break;
	}

	if(g_RunState != sWaitingTimer)
		checkJoggerMovements();
}


void enc2mdb_init(ENC2MDB_MODBUSSLAVE* self, enum MOUNT_ROTATION mountRotation)
{
	enc2mdb_button_init();
	enc2mdb_rot_init(mountRotation);
	enc2mdb_adc_init(mountRotation);
	g_MountRotation = mountRotation;

	if(g_MountRotation == cwise)
		g_StartingPixel = 18U;
	else if(g_MountRotation == ccwise)
		g_StartingPixel = 6U;

	enc2mdb_jogger_initNeopixel();

	g_RotatingPixelTimerHandle = xTimerCreate("RotatingPixelTimer",
    							 	 ROT_PIXEL_TIMER_PERIOD_MS,
									 pdFALSE,
									 0,
									 rotatingPixelTimerCallback);

	g_ExitWaitTimerHandle = xTimerCreate("ExitFromWaitTimer",
									 EXIT_WAIT_TIMER_PERIOD_MS,
									 pdFALSE,
									 0,
									 exitFromWaitTimerCallback);

	g_WaitBlinkTimerHandle = xTimerCreate("WaitBlinkTimer",
									 WAIT_BLINK_PERIOD_MS,
									 pdFALSE,
									 0,
									 exitFromWaitTimerCallback);

	enc2mdb_neopixring_uart_send_fw_ver_req();
}
