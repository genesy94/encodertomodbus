/*
 * enc2mdb_types.h
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */

#ifndef APP_ENC2MDB_TYPES_H_
#define APP_ENC2MDB_TYPES_H_


enum JOGGER_EVENT
{
	evNone,
	evPush,
	evUp,
	evDown,
	evLeft,
	evRight,
	evUpLeft,
	evUpRight,
	evDownLeft,
	evDownRight,
	evRot
};


enum NEOPIXEL_RING_MODE
{
	modeFollowMoveOneColor,
	modeOnOneColor,
	modeOff,
	modeRotatingOneColor,
	modeFlashingOneColor,
	modeFollowMoveColorPerPixel,
	modeOnColorPerPixel,
	modeRotatingColorPerPixel,
	modeFlashingColorPerPixel
};


enum NEOPIXEL_RING_SPEED
{
	speedSlow,
	speedNormal,
	speedFast
};


enum MOUNT_ROTATION
{
	none,
	cwise,
	ccwise
};


typedef uint16_t (*const ENC2MDB_GETONE_MDBREQ)(uint8_t *inputStream, uint16_t streamLength, uint8_t* request);
typedef uint16_t (*const ENC2MDB_MODBUSSLAVE_PARSE)(const uint8_t *request, const uint16_t requestLength, uint8_t *response);


typedef struct MdbSlaveCfg_t
{
	uint16_t	port;
} ENC2MDB_MODBUSSLAVE_CFG;


typedef struct enc2mdbModbusSlave_t
{
	ENC2MDB_MODBUSSLAVE_CFG 	cfg;
	ENC2MDB_GETONE_MDBREQ		getMdbRequestFromStream;
	ENC2MDB_MODBUSSLAVE_PARSE	mdbParseRequest;
}ENC2MDB_MODBUSSLAVE;


typedef struct ENC2MDBEncoderValue_t
{
	uint32_t   	value;
    uint8_t 	direction;
} ENC2MDB_ENCODER_VALUE;


typedef struct ENC2MDBRGBValue_t
{
	int16_t   	colorR;
	int16_t 	colorG;
	int16_t 	colorB;
} ENC2MDB_RGB_VALUE;


typedef struct ENC2MDBRGBDimmingStep_t
{
	double   	dimmingStepR;
	double 		dimmingStepG;
	double 		dimmingStepB;
} ENC2MDB_RGB_DIMMING_STEP;


enum MDB_EVENT
{
	noEvent,
	jogMode,
	jogSpeed,
	generalColor,
	singleEvenPixelColor,
	singleOddPixelColor,
	singlePixelOnOff
};


enum RUN_STATE
{
	sIdle,
	sSinglePixWait,
	sSinglePixEvenColUpd,
	sSinglePixOddColUpd,
	sSinglePixelOnOffUpd,
	sWaitingTimer,
	sManageWaitingEvent
};

#endif /* APP_ENC2MDB_TYPES_H_ */
