/*
 * enc2mdb_fw_version.h
 *
 *  Created on: 4 Feb 2022
 *      Author: dario
 */

#ifndef APP_ENC2MDB_FW_VERSION_H_
#define APP_ENC2MDB_FW_VERSION_H_


#define	FW_VERSION_MAJOR_NUM	2U
#define	FW_VERSION_MINOR_NUM	0U
#define	FW_VERSION_PATCH_NUM	5U


#endif /* APP_ENC2MDB_FW_VERSION_H_ */
