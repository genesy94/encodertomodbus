/*
 * enc2mdb.h
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */

#ifndef APP_ENC2MDB_H_
#define APP_ENC2MDB_H_

#define NEW_ENC2MDB_MODBUSSLAVE(getMdbRequestFromStream, mdbParseRequest)		\
{ 																				\
	{502U},																		\
	(getMdbRequestFromStream),													\
	(mdbParseRequest)															\
}

extern void enc2mdb_run(ENC2MDB_MODBUSSLAVE* self);
extern void enc2mdb_init(ENC2MDB_MODBUSSLAVE* self, enum MOUNT_ROTATION mountRotation);

#endif /* APP_ENC2MDB_H_ */
