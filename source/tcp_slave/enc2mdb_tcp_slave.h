/*
 * enc2mdb_tcp_slave.h
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */

#ifndef TCP_SLAVE_ENC2MDB_TCP_SLAVE_H_
#define TCP_SLAVE_ENC2MDB_TCP_SLAVE_H_

extern void enc2mdb_TcpSlaveInit(ENC2MDB_MODBUSSLAVE* self, ENC2MDB_MODBUSSLAVE_CFG* cfg);

#endif /* TCP_SLAVE_ENC2MDB_TCP_SLAVE_H_ */
