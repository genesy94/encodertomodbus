/*
 * enc2mdb_tcp_slave.c
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */


#include "lwip/opt.h"
#include "lwip/api.h"
#include "lwip/sys.h"
#include "enc2mdb_types.h"
#include "enc2mdb_tcp_slave.h"
#include "conf_threads.h"
#include <stdio.h>
#include <string.h>
#include "netif/ethernet.h"
#include "enet_ethernetif.h"
#include "lwip/sockets.h"
#include "stdbool.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#ifndef MODBUS_TCP_ADU_MAX
#define MODBUS_TCP_ADU_MAX	260
#endif

#define	RX_BUFFER_DIM		1400

#define KEEPALIVE_ON		1
#define KEEPALIVE_COUNT		3
#define KEEPALIVE_IDLE		1
#define KEEPALIVE_INTVL		1


typedef struct TcpTaskParameters_t {
    uint16_t port;
    ENC2MDB_MODBUSSLAVE* self;
} TCP_TASK_PARAMETERS;


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static TCP_TASK_PARAMETERS paramsPrimary, paramsSecondary;

/*******************************************************************************
 * Code
 ******************************************************************************/
static void enc2mdb_TcpSlave_task(void *arg)
{
    int len, reqLen = 0, respLen, ip_protocol = 0, listen_sock, sock;
	int keepAlive = KEEPALIVE_ON, keepIdle = KEEPALIVE_IDLE, keepInterval = KEEPALIVE_INTVL, keepCount = KEEPALIVE_COUNT;
	char addr_str[128], rx_buffer[RX_BUFFER_DIM];
	struct sockaddr_storage source_addr, dest_addr;
	uint8_t modbusReq[MODBUS_TCP_ADU_MAX], modbusResp[MODBUS_TCP_ADU_MAX];
    bool initDone = false;
    socklen_t addr_len;

	struct sockaddr_in *dest_addr_ip4 = (struct sockaddr_in *)&dest_addr;
	TCP_TASK_PARAMETERS* params = ((TCP_TASK_PARAMETERS*)arg);

	dest_addr_ip4->sin_addr.s_addr = htonl(INADDR_ANY);
	dest_addr_ip4->sin_family = AF_INET;
	dest_addr_ip4->sin_port = htons(params->port);
	ip_protocol = IPPROTO_IP;

	while (1) {
		if(initDone == false) {
			listen_sock = socket(AF_INET, SOCK_STREAM, ip_protocol);
			if (listen_sock < 0) {
				continue;
			}
			int opt = 1;
			setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

			int err = bind(listen_sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
			if (err != 0) {
				close(listen_sock);
				continue;
			}

			err = listen(listen_sock, 1);
			if (err != 0) {
				close(listen_sock);
				continue;
			}
			initDone = true;
		}

		socklen_t addr_len = sizeof(source_addr);
		sock = accept(listen_sock, (struct sockaddr *)&source_addr, &addr_len);
		if (sock < 0) {
			close(listen_sock);
			initDone = false;
			continue;
		}

		// Set tcp keepalive option
		setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, &keepAlive, sizeof(int));
		setsockopt(sock, IPPROTO_TCP, TCP_KEEPIDLE, &keepIdle, sizeof(int));
		setsockopt(sock, IPPROTO_TCP, TCP_KEEPINTVL, &keepInterval, sizeof(int));
		setsockopt(sock, IPPROTO_TCP, TCP_KEEPCNT, &keepCount, sizeof(int));
		// Convert ip address to string
		if (source_addr.ss_family == PF_INET) {
			inet_ntoa_r(((struct sockaddr_in *)&source_addr)->sin_addr, addr_str, sizeof(addr_str) - 1);
		}

		while ((len = recv(sock, rx_buffer, sizeof(rx_buffer) - 1, 0)) >= 0) {
			do {
				if((reqLen = params->self->getMdbRequestFromStream(rx_buffer, len, modbusReq)) > 0) {
					len -= reqLen;
					if((respLen = params->self->mdbParseRequest(modbusReq, reqLen, modbusResp)) > 0)
						send(sock, modbusResp, respLen, 0);
				}
			} while ((len > 0) && (reqLen > 0));
		}

		shutdown(sock, 0);
		close(sock);
	}

	close(listen_sock);
	vTaskDelete(NULL);
}


void enc2mdb_TcpSlaveInit(ENC2MDB_MODBUSSLAVE* self, ENC2MDB_MODBUSSLAVE_CFG* cfg)
{
	if (cfg != NULL) {
		self->cfg.port = cfg->port;
	}

	paramsPrimary.port = self->cfg.port;
	paramsPrimary.self = self;
	paramsSecondary.port = self->cfg.port + 1;
	paramsSecondary.self = self;

	if (sys_thread_new("tcpSlave_task_pri", enc2mdb_TcpSlave_task, (void*)&paramsPrimary, TCP_SLAVE_STACK_SIZE, TCP_SLAVE_TASK_PRIORITY) == NULL) {
	  LWIP_ASSERT("lwip: tcpSlave_task_pri Task creation failed.", 0);
	}

	if (sys_thread_new("tcpSlave_task_sec", enc2mdb_TcpSlave_task, (void*)&paramsSecondary, TCP_SLAVE_STACK_SIZE, TCP_SLAVE_TASK_PRIORITY) == NULL) {
	  LWIP_ASSERT("lwip: tcpSlave_task_sec Task creation failed.", 0);
	}
}
