/*
 * Copyright 2016-2021 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    EncoderToModbusNew.c
 * @brief   Application entry point.
 */

#include "lwip/opt.h"
#include "lwip/netifapi.h"
#include "lwip/tcpip.h"
#include "netif/ethernet.h"
#include "enet_ethernetif.h"
#include "lwip/netif.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "board.h"

#include "nvram.h"

#include "lwip_stack_init.h"
#include "BasicTFTP.h"

#include "enc2mdb_modbus.h"
#include "enc2mdb_types.h"
#include "enc2mdb_tcp_slave.h"
#include "enc2mdb_neopixring_uart.h"
#include "enc2mdb.h"

#include "conf_threads.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define	RUN_TIMER_MS	10
#define	GROUNDED		0U

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
//flash data
nvramData_t flashNvram;
ENC2MDB_MODBUSSLAVE_CFG cfg = {502U};
ENC2MDB_MODBUSSLAVE modbusSlaveContext = NEW_ENC2MDB_MODBUSSLAVE(enc2mdb_getOneModbusRequest, enc2mdb_modbusSlaveParseRequest);
static bool gConnected = true;

/*******************************************************************************
 * Code
 ******************************************************************************/
static void main_task(void *param)
{
	 TickType_t xLastWakeTime;
	 xLastWakeTime = xTaskGetTickCount();
	 for(;;)
	 {
		 vTaskDelayUntil(&xLastWakeTime, RUN_TIMER_MS/portTICK_PERIOD_MS);
		 enc2mdb_run(&modbusSlaveContext);
	 }
}


int main(void)
{
    SYSMPU_Type *base = SYSMPU;
	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitDebugConsole();
	enum MOUNT_ROTATION mountRotation = none;

    /* Disable SYSMPU. */
    base->CESR &= ~SYSMPU_CESR_VLD_MASK;

    NVR_init(&flashNvram);//init flash structure

    lwip_stack_init();
    tftpd_init();

    enc2mdb_neopixring_uart_init();
    enc2mdb_modbusSlaveInit();
    enc2mdb_TcpSlaveInit(&modbusSlaveContext, &cfg);
    if(GPIO_PinRead(BOARD_INITPINS_ROT90CCW_GPIO, BOARD_INITPINS_ROT90CCW_PIN) == GROUNDED)
    	mountRotation = ccwise;
    else if(GPIO_PinRead(BOARD_INITPINS_ROT90CW_GPIO, BOARD_INITPINS_ROT90CW_PIN) == GROUNDED)
    	mountRotation = cwise;
    enc2mdb_init(&modbusSlaveContext, mountRotation);

    if (sys_thread_new("main_task", main_task, NULL, MAIN_STACK_SIZE, MAIN_TASK_PRIORITY) == NULL)
    {
        LWIP_ASSERT("main(): Task creation failed.", 0);
    }

    vTaskStartScheduler();

	for(;;){}

	return 0;
}

