/*
 * enc2mdb_rot.h
 *
 *  Created on: 30 Sep 2021
 *      Author: dario
 */

#ifndef CLASSIC_ENCODER_ENC2MDB_ROT_H_
#define CLASSIC_ENCODER_ENC2MDB_ROT_H_

extern void enc2mdb_rot_run(enum JOGGER_EVENT *event, uint8_t *rotatingPixel);
extern void enc2mdb_rot_init(enum MOUNT_ROTATION mountRotation);

#endif /* CLASSIC_ENCODER_ENC2MDB_ROT_H_ */
