/*
 * enc2mdb_button.c
 *
 *  Created on: 30 Sep 2021
 *      Author: dario
 */

#include "fsl_debug_console.h"
#include "fsl_port.h"
#include "fsl_gpio.h"
#include "fsl_common.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "board.h"
#include "enc2mdb_jogger.h"
#include "enc2mdb_types.h"
#include "enc2mdb_neopixring_uart.h"
#include "timers.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define BOARD_SW_GPIO        BOARD_INITPINS_ENC_BUTT_GPIO
#define BOARD_SW_PORT        BOARD_INITPINS_ENC_BUTT_PORT
#define BOARD_SW_GPIO_PIN    BOARD_INITPINS_ENC_BUTT_PIN
#define BOARD_SW_IRQ         PORTC_IRQn
#define BOARD_SW_IRQ_HANDLER PORTC_IRQHandler

#define BUTTON_PRESSED		0U
#define BUTTON_UNPRESSED	1U

#define DOUBLE_PUSH_TIMER_PERIOD_MS (300 / portTICK_PERIOD_MS)


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static const gpio_pin_config_t g_SwConfig = {kGPIO_DigitalInput, 0,};
static volatile uint32_t g_ButtonPress = BUTTON_UNPRESSED;
static TimerHandle_t g_DoublePushTimerHandle = NULL;
static bool g_DoublePushTimerStarted = false;
static bool g_SecondPushArrived = false;

/*******************************************************************************
 * Code
 ******************************************************************************/
static void doublePushTimerCallback(TimerHandle_t xTimer)
{
	if(g_SecondPushArrived == true) {
    	enc2mdb_jogger_addDoublePush(1);
		g_SecondPushArrived = false;
	}
	else {
    	enc2mdb_jogger_addPush(1);
	}
	g_DoublePushTimerStarted = false;
}


void enc2mdb_button_run(enum JOGGER_EVENT *event)
{
	uint32_t pressValue;
	*event = evNone;
	pressValue = GPIO_PinRead(BOARD_SW_GPIO, BOARD_SW_GPIO_PIN);
    if (pressValue == BUTTON_PRESSED) {
    	*event = evPush;
    	if(g_ButtonPress == BUTTON_UNPRESSED) {
			if(g_DoublePushTimerStarted == false) {
				xTimerStart(g_DoublePushTimerHandle, 0);
				g_DoublePushTimerStarted = true;
			}
			else {
				g_SecondPushArrived = true;
			}
			enc2mdb_jogger_setPushFlag();
    	}
    }
    else if (pressValue == BUTTON_UNPRESSED && g_ButtonPress == BUTTON_PRESSED) {
		enc2mdb_jogger_resetPushFlag();
    }
	g_ButtonPress = pressValue;
}


void enc2mdb_button_init()
{
    GPIO_PinInit(BOARD_SW_GPIO, BOARD_SW_GPIO_PIN, &g_SwConfig);
	g_DoublePushTimerHandle = xTimerCreate("DoublePushTimer",
								 DOUBLE_PUSH_TIMER_PERIOD_MS,
                                 pdFALSE,
                                 0,
								 doublePushTimerCallback);
}
