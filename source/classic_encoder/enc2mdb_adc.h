/*
 * enc2mdb_adc.h
 *
 *  Created on: 14 Sep 2021
 *      Author: dario
 */

#ifndef CLASSIC_ENCODER_ENC2MDB_ADC_H_
#define CLASSIC_ENCODER_ENC2MDB_ADC_H_

extern void enc2mdb_adc_run(enum JOGGER_EVENT *event);
extern void enc2mdb_adc_init(enum MOUNT_ROTATION mountRotation);

#endif /* CLASSIC_ENCODER_ENC2MDB_ADC_H_ */
