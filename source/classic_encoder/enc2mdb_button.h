/*
 * enc2mdb_button.h
 *
 *  Created on: 30 Sep 2021
 *      Author: dario
 */

#ifndef CLASSIC_ENCODER_ENC2MDB_BUTTON_H_
#define CLASSIC_ENCODER_ENC2MDB_BUTTON_H_

extern void enc2mdb_button_run(enum JOGGER_EVENT *event);
extern void enc2mdb_button_init();

#endif /* CLASSIC_ENCODER_ENC2MDB_BUTTON_H_ */
