/*
 * enc2mdb_adc.c
 *
 *  Created on: 14 Sep 2021
 *      Author: dario
 */

#include "fsl_adc16.h"
#include "fsl_debug_console.h"
#include "enc2mdb_jogger.h"
#include "enc2mdb_types.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define ENC_LR_ADC16_BASE       		ADC0
#define ENC_UD_ADC16_BASE       		ADC1
#define ADC16_CHANNEL_GROUP				0U
#define ADC16_ENCODER_CHANNEL  			4U

#define UP_RIGHT_LOW_REF_VALUE			1800U
#define UP_RIGHT_HIGH_REF_VALUE			2800U

#define DOWN_LEFT_LOW_REF_VALUE			1290U
#define DOWN_LEFT_HIGH_REF_VALUE		290U

#define MOVE_SCALE						100U

#define DOUBLE_MOVE_FLAG_PERC_ACT		0.5


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static const uint32_t g_Adc16_12bitFullRange = 4096U;
static adc16_config_t g_Adc16ConfigStruct;
static adc16_channel_config_t g_Adc16ChannelConfigStruct;

static uint32_t g_LeftRightAdcValue;
static uint32_t g_UpDownAdcValue;

static enum MOUNT_ROTATION g_MountRotation;

typedef enum
{
	noMove = -1,
	moveUp,
	moveDown,
	moveLeft,
	moveRight
} DIR_DETECTED;

static const ENC2MDB_SET_MOVE_VALUE_FUNC g_MoveSetMoveFuncs[4] = {&enc2mdb_jogger_setMoveUpValue,
															 	 &enc2mdb_jogger_setMoveDownValue,
																 &enc2mdb_jogger_setMoveLeftValue,
																 &enc2mdb_jogger_setMoveRightValue};

static const ENC2MDB_SET_FLAG_FUNC g_MoveSetFlagsFuncs[4] = {&enc2mdb_jogger_setMoveUpFlag,
															 &enc2mdb_jogger_setMoveDownFlag,
															 &enc2mdb_jogger_setMoveLeftFlag,
															 &enc2mdb_jogger_setMoveRightFlag};

static const ENC2MDB_SET_FLAG_FUNC g_MoveResetFlagsFuncs[4] = {&enc2mdb_jogger_resetMoveUpFlag,
															   &enc2mdb_jogger_resetMoveDownFlag,
															   &enc2mdb_jogger_resetMoveLeftFlag,
															   &enc2mdb_jogger_resetMoveRightFlag};

static const DIR_DETECTED g_TraslateDirCw[4] = {moveRight, moveLeft, moveUp, moveDown};
static const DIR_DETECTED g_TraslateDirCCw[4] = {moveLeft, moveRight, moveDown, moveUp};

static const uint8_t g_LedRingMoveEvents[4] = {evUp, evDown, evLeft, evRight};

static DIR_DETECTED g_PrimaryDir = noMove;
static DIR_DETECTED g_SecondaryDir = noMove;
static uint16_t g_MoveValuePrimaryDir = 0U;
static uint16_t g_MoveValueSecondaryDir = 0U;

static bool	g_MoveDetectedLastTime = false;


/*******************************************************************************
 * Code
 ******************************************************************************/
static void checkDirs(DIR_DETECTED possibleNewDir, uint16_t moveValue) {
	if(g_PrimaryDir == noMove) {
		g_PrimaryDir = possibleNewDir;
		g_MoveValuePrimaryDir = moveValue;
	}
	else if (g_SecondaryDir == noMove) {
		if(moveValue > g_MoveValuePrimaryDir) {
			g_SecondaryDir = g_PrimaryDir;
			g_MoveValueSecondaryDir = g_MoveValuePrimaryDir;
			g_PrimaryDir = possibleNewDir;
			g_MoveValuePrimaryDir = moveValue;
		}
		else {
			g_SecondaryDir = possibleNewDir;
			g_MoveValueSecondaryDir = moveValue;
		}
	}
}


static DIR_DETECTED rotateDirIndex(DIR_DETECTED inputDir)
{
	if(g_MountRotation == cwise)
		return g_TraslateDirCw[inputDir];
	if(g_MountRotation == ccwise)
		return g_TraslateDirCCw[inputDir];
    return inputDir;
}


static void adcInit(ADC_Type *adcBase, adc16_config_t *adcConfig)
{
	ADC16_Init(adcBase, adcConfig);
	ADC16_SetChannelMuxMode(adcBase, kADC16_ChannelMuxB);
	ADC16_EnableHardwareTrigger(adcBase, false);
	#if defined(FSL_FEATURE_ADC16_HAS_CALIBRATION) && FSL_FEATURE_ADC16_HAS_CALIBRATION
	assert(kStatus_Success == ADC16_DoAutoCalibration(adcBase));
	#endif /* FSL_FEATURE_ADC16_HAS_CALIBRATION */
}


static void runConversion(ADC_Type *adcBase, uint32_t *conversionValueRead)
{
    ADC16_SetChannelConfig(adcBase, ADC16_CHANNEL_GROUP, &g_Adc16ChannelConfigStruct);
    while (0U == (kADC16_ChannelConversionDoneFlag &
                  ADC16_GetChannelStatusFlags(adcBase, ADC16_CHANNEL_GROUP)))
    {
    }
   *conversionValueRead = ADC16_GetChannelConversionValue(adcBase, ADC16_CHANNEL_GROUP);
}


void enc2mdb_adc_run(enum JOGGER_EVENT *event)
{
	uint16_t moveValueTemp;
	bool moveDetected = false;
	*event = evNone;
	runConversion(ENC_LR_ADC16_BASE, &g_LeftRightAdcValue);
    if(g_LeftRightAdcValue > UP_RIGHT_LOW_REF_VALUE) {
    	moveValueTemp = ((g_LeftRightAdcValue - UP_RIGHT_LOW_REF_VALUE) * MOVE_SCALE) / (UP_RIGHT_HIGH_REF_VALUE - UP_RIGHT_LOW_REF_VALUE);
		(*g_MoveSetMoveFuncs[rotateDirIndex(moveRight)])(moveValueTemp);
    	checkDirs(moveRight, moveValueTemp);
    }
    else {
    	(*g_MoveResetFlagsFuncs[rotateDirIndex(moveRight)])();
    	(*g_MoveSetMoveFuncs[rotateDirIndex(moveRight)])(0U);
    }

    if(g_LeftRightAdcValue < DOWN_LEFT_LOW_REF_VALUE) {
    	moveValueTemp = ((DOWN_LEFT_LOW_REF_VALUE - g_LeftRightAdcValue) * MOVE_SCALE) / (DOWN_LEFT_LOW_REF_VALUE - DOWN_LEFT_HIGH_REF_VALUE);
    	(*g_MoveSetMoveFuncs[rotateDirIndex(moveLeft)])(moveValueTemp);
    	checkDirs(moveLeft, moveValueTemp);
    }
    else {
    	(*g_MoveResetFlagsFuncs[rotateDirIndex(moveLeft)])();
    	(*g_MoveSetMoveFuncs[rotateDirIndex(moveLeft)])(0U);
    }

    runConversion(ENC_UD_ADC16_BASE, &g_UpDownAdcValue);
    if(g_UpDownAdcValue > UP_RIGHT_LOW_REF_VALUE) {
    	moveValueTemp = ((g_UpDownAdcValue - UP_RIGHT_LOW_REF_VALUE) * MOVE_SCALE) / (UP_RIGHT_HIGH_REF_VALUE - UP_RIGHT_LOW_REF_VALUE);
    	(*g_MoveSetMoveFuncs[rotateDirIndex(moveUp)])(moveValueTemp);
    	checkDirs(moveUp, moveValueTemp);
    }
    else {
    	(*g_MoveResetFlagsFuncs[rotateDirIndex(moveUp)])();
    	(*g_MoveSetMoveFuncs[rotateDirIndex(moveUp)])(0U);
    }

    if(g_UpDownAdcValue < DOWN_LEFT_LOW_REF_VALUE) {
    	moveValueTemp = ((DOWN_LEFT_LOW_REF_VALUE - g_UpDownAdcValue) * MOVE_SCALE) / (DOWN_LEFT_LOW_REF_VALUE - DOWN_LEFT_HIGH_REF_VALUE);
    	(*g_MoveSetMoveFuncs[rotateDirIndex(moveDown)])(moveValueTemp);
    	checkDirs(moveDown, moveValueTemp);
    }
    else {
    	(*g_MoveResetFlagsFuncs[rotateDirIndex(moveDown)])();
    	(*g_MoveSetMoveFuncs[rotateDirIndex(moveDown)])(0U);
    }

    if(g_PrimaryDir != noMove) {
    	g_MoveDetectedLastTime = true;
    	(*g_MoveSetFlagsFuncs[rotateDirIndex(g_PrimaryDir)])();
        if(g_SecondaryDir != noMove) {
        	if(g_MoveValueSecondaryDir > g_MoveValuePrimaryDir * DOUBLE_MOVE_FLAG_PERC_ACT) {
            	(*g_MoveSetFlagsFuncs[rotateDirIndex(g_SecondaryDir)])();
            	if(g_LedRingMoveEvents[g_PrimaryDir] + g_LedRingMoveEvents[g_SecondaryDir] == evUp + evLeft)
            		*event = evUpLeft;
            	else if((g_LedRingMoveEvents[g_PrimaryDir] + g_LedRingMoveEvents[g_SecondaryDir] == evUp + evRight) &&
            			(g_LedRingMoveEvents[g_PrimaryDir] == evUp || g_LedRingMoveEvents[g_SecondaryDir] == evUp))
            		*event = evUpRight;
            	else if((g_LedRingMoveEvents[g_PrimaryDir] + g_LedRingMoveEvents[g_SecondaryDir] == evDown + evLeft) &&
            			(g_LedRingMoveEvents[g_PrimaryDir] == evDown || g_LedRingMoveEvents[g_SecondaryDir] == evDown))
            		*event = evDownLeft;
            	else
            		*event = evDownRight;
        	}
			else {
				(*g_MoveResetFlagsFuncs[rotateDirIndex(g_SecondaryDir)])();
	        	*event = g_LedRingMoveEvents[g_PrimaryDir];
			}
        }
        else {
        	*event = g_LedRingMoveEvents[g_PrimaryDir];
        }
    }
    else {
    	if(g_MoveDetectedLastTime == true) {
    		*event = evNone;
    		g_MoveDetectedLastTime = false;
    	}
    }

    g_PrimaryDir = noMove;
    g_SecondaryDir = noMove;
    g_MoveValuePrimaryDir = 0U;
    g_MoveValueSecondaryDir = 0U;
}


void enc2mdb_adc_init(enum MOUNT_ROTATION mountRotation) {
	g_Adc16ConfigStruct.referenceVoltageSource = kADC16_ReferenceVoltageSourceVref;
	g_Adc16ConfigStruct.clockSource = kADC16_ClockSourceAsynchronousClock;
	g_Adc16ConfigStruct.enableAsynchronousClock = true;
	g_Adc16ConfigStruct.clockDivider = kADC16_ClockDivider8;
	g_Adc16ConfigStruct.resolution = kADC16_ResolutionSE16Bit;
	g_Adc16ConfigStruct.longSampleMode = kADC16_LongSampleDisabled;
	g_Adc16ConfigStruct.enableHighSpeed = false;
	g_Adc16ConfigStruct.enableLowPower = false;
	g_Adc16ConfigStruct.enableContinuousConversion = false;

	ADC16_GetDefaultConfig(&g_Adc16ConfigStruct);

	adcInit(ENC_LR_ADC16_BASE, &g_Adc16ConfigStruct);
	adcInit(ENC_UD_ADC16_BASE, &g_Adc16ConfigStruct);

	g_Adc16ChannelConfigStruct.channelNumber  = ADC16_ENCODER_CHANNEL;
	g_Adc16ChannelConfigStruct.enableInterruptOnConversionCompleted = false;
	#if defined(FSL_FEATURE_ADC16_HAS_DIFF_MODE) && FSL_FEATURE_ADC16_HAS_DIFF_MODE
	g_Adc16ChannelConfigStruct.enableDifferentialConversion = false;
	#endif /* FSL_FEATURE_ADC16_HAS_DIFF_MODE */

	g_MountRotation = mountRotation;
}
