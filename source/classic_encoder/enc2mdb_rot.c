/*
 * enc2mdb_rot.c
 *
 *  Created on: 30 Sep 2021
 *      Author: dario
 */

#include "fsl_debug_console.h"
#include "pin_mux.h"
#include "board.h"
#include "fsl_ftm.h"
#include "fsl_pit.h"
#include "enc2mdb_jogger.h"
#include "enc2mdb_jogger_neopixel.h"
#include "enc2mdb_types.h"
#include "enc2mdb_neopixring_uart.h"
#include "PacketBuffer.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define PIT_IRQ_PRIOR				4U
#define PIT_IRQ_HANDLER		 		PIT0_IRQHandler
#define PIT_IRQ_ID      			PIT0_IRQn
/* Get source clock for PIT driver */
#define PIT_SOURCE_CLOCK 			CLOCK_GetFreq(kCLOCK_BusClk)

/* The Flextimer instance/channel used for board */
#define FTM_BASEADDR 				FTM2

/* Get source clock for FTM driver */
#define FTM_SOURCE_CLOCK 			CLOCK_GetFreq(kCLOCK_BusClk)
#define QUAD_DECODER_MODULO 		127U

#define ENCODER_UPDATE_TIMER_MSEC	10U

#define ENCODER_DIR_CLOCK			1U
#define ENCODER_DIR_COUNTERCLOCK	0U

#define NUM_OF_PIXELS				24U
#define UINT8T_MAX_VALUE			255U

#define UNUSED_PARAM				0U


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
static ftm_config_t g_FtmInfo;
static ftm_phase_params_t g_PhaseParamsConfigStruct;
volatile uint32_t g_LoopCounter = 0U;
static ENC2MDB_ENCODER_VALUE g_LastEncoderValue;
volatile bool g_FirstRead = true;
static enum MOUNT_ROTATION g_MountRotation;

/*******************************************************************************
 * Code
 ******************************************************************************/
static void PIT_Configuration(void)
{
    pit_config_t pitConfig;

    PIT_GetDefaultConfig(&pitConfig);
    /* Init pit module */
    PIT_Init(PIT, &pitConfig);

    /* Set timer period for channel 0 */
    PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(1000U, PIT_SOURCE_CLOCK));

    /* Enable timer interrupts for channel 0 */
    PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);

    /* Enable at the NVIC */
    EnableIRQ(PIT_IRQ_ID);
}


void PIT_IRQ_HANDLER(void)
{
    /* Clear pit interrupt flag */
    PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag);

    g_LoopCounter++;
    if (g_LoopCounter > ENCODER_UPDATE_TIMER_MSEC)
    {
    	g_LoopCounter = 0U;
    	/* Read counter value */
    	g_LastEncoderValue.value = FTM_GetQuadDecoderCounterValue(FTM_BASEADDR);
    	/* Clear counter */
    	FTM_ClearQuadDecoderCounterValue(FTM_BASEADDR);
    	/* Read direction */
    	if (FTM_GetQuadDecoderFlags(FTM_BASEADDR) & kFTM_QuadDecoderCountingIncreaseFlag)
    	{
    		g_LastEncoderValue.direction = ENCODER_DIR_CLOCK;
    	}
    	else
    	{
    		g_LastEncoderValue.direction = ENCODER_DIR_COUNTERCLOCK;
    	}

    	if((g_LastEncoderValue.value != 0) && (g_FirstRead == false))
    	{
    		PBUF_addEncoderPacket(&g_LastEncoderValue);
    	}

    	if(g_FirstRead == true)
    	{
    		g_FirstRead = false;
    	}
    }

    SDK_ISR_EXIT_BARRIER;
}


void enc2mdb_rot_run(enum JOGGER_EVENT *event, uint8_t *rotatingPixel)
{
	uint8_t lastRingRotatingPixel;
	ENC2MDB_ENCODER_VALUE valueRead;
	*event = evNone;
	while(PBUF_getEncoderPacket((ENC2MDB_ENCODER_VALUE*) &valueRead) == pdTRUE) {
		lastRingRotatingPixel = (uint8_t)enc2mdb_jogger_getNeopixelRotatingPixel(UNUSED_PARAM);
	    if (valueRead.direction == ENCODER_DIR_CLOCK)
	    {
	    	valueRead.value &= 0x007F;
	    	lastRingRotatingPixel = (lastRingRotatingPixel + (uint8_t)valueRead.value) % NUM_OF_PIXELS;
	        enc2mdb_jogger_addRotation(valueRead.value);
	    }
	    else
	    {
	    	valueRead.value = 0x0080 - valueRead.value;
	    	lastRingRotatingPixel -= (uint8_t)valueRead.value;
		    if((lastRingRotatingPixel >= NUM_OF_PIXELS) && (lastRingRotatingPixel <= UINT8T_MAX_VALUE))
		    	lastRingRotatingPixel = (NUM_OF_PIXELS - 1) - (UINT8T_MAX_VALUE - lastRingRotatingPixel);
	        enc2mdb_jogger_addRotation(-valueRead.value);
	    }
	    *event = evRot;
	    *rotatingPixel = lastRingRotatingPixel;
	}
}


void enc2mdb_rot_init(enum MOUNT_ROTATION mountRotation)
{
	//packet buffer init
	PBUF_init();
	//set priority of PIT timer channel 0 IRQ >= 2
	NVIC_SetPriority(PIT0_IRQn, PIT_IRQ_PRIOR);

	/* PIT configuration */
    PIT_Configuration();

    /* Initialize FTM module */
    FTM_GetDefaultConfig(&g_FtmInfo);
    g_FtmInfo.prescale = kFTM_Prescale_Divide_1;
    FTM_Init(FTM_BASEADDR, &g_FtmInfo);

    /* Set the modulo values for Quad Decoder. */
    FTM_SetQuadDecoderModuloValue(FTM_BASEADDR, 0U, QUAD_DECODER_MODULO);

    /* Enable the Quad Decoder mode. */
    g_PhaseParamsConfigStruct.enablePhaseFilter = true;
    g_PhaseParamsConfigStruct.phaseFilterVal    = 0U;
    g_PhaseParamsConfigStruct.phasePolarity     = kFTM_QuadPhaseNormal;
    FTM_SetupQuadDecode(FTM_BASEADDR, &g_PhaseParamsConfigStruct,
                        &g_PhaseParamsConfigStruct,
                        kFTM_QuadPhaseEncode);

    PIT_StartTimer(PIT, kPIT_Chnl_0);

	g_MountRotation = mountRotation;
}
