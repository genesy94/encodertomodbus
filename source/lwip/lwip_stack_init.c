/*
 * lwip_stack_init.c
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */
#include "lwip/opt.h"

#include "lwip/tcpip.h"
#include "lwip/netifapi.h"
#include "lwip/netif.h"
#include "netif/ethernet.h"
#include "enet_ethernetif.h"

#include "board.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "conf_eth.h"
#include "BasicTFTP.h"

#include "nvram.h"
#include "parsing.h"

#include "fsl_phy.h"
#include "fsl_phyksz8081.h"
#include "fsl_enet_mdio.h"
#include "fsl_device_registers.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
* Prototypes
******************************************************************************/

/*******************************************************************************
* Variables
******************************************************************************/
//flash data
extern nvramData_t flashNvram;

extern unsigned char MacAddress[6];
extern unsigned char IpAddress[4];
extern unsigned char ManagerIpAddress[4];

static mdio_handle_t mdioHandle = {.ops = &enet_ops};
static phy_handle_t phyHandle   = {.phyAddr = BOARD_ENET0_PHY_ADDRESS, .mdioHandle = &mdioHandle, .ops = &phyksz8081_ops};


/*******************************************************************************
 * Code
 ******************************************************************************/
void lwip_stack_init()
{
	char flashContent[NVRAM_SIZE_BYTES];
	char cont2[NVRAM_SIZE_BYTES];
    ip4_addr_t netif_ipaddr, netif_netmask, netif_gw;
    ethernetif_config_t enet_config;
    struct netif netif;

    mdioHandle.resource.csrClock_Hz = CLOCK_GetFreq(kCLOCK_CoreSysClk);

    /* Read ip address from flash */
    memcpy(flashContent, (void*)(flashNvram.nvram_data), NVRAM_SIZE_BYTES);
    parsefile(flashContent);
	parsestring(MAC);
	parsestring(IP);
	parsestring(MANAGER);

	enet_config.phyHandle = &phyHandle;
	memcpy(enet_config.macAddress, (void*)(MacAddress), sizeof(MacAddress));

	/* Default address. */
    IP4_ADDR(&netif_ipaddr, IpAddress[0], IpAddress[1], IpAddress[2], IpAddress[3]);
    /* Default Subnet mask. */
    IP4_ADDR(&netif_netmask, configNET_MASK0, configNET_MASK1, configNET_MASK2, configNET_MASK3);
    /* Default Gw addr. */
    IP4_ADDR(&netif_gw, configGW_ADDR0, configGW_ADDR1, configGW_ADDR2, configGW_ADDR3);

    tcpip_init(NULL, NULL);

    netifapi_netif_add(&netif, &netif_ipaddr, &netif_netmask, &netif_gw, &enet_config, ethernetif0_init,
                       tcpip_input);
    netifapi_netif_set_default(&netif);
    netifapi_netif_set_up(&netif);
}

