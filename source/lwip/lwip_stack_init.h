/*
 * lwip_stack_init.h
 *
 *  Created on: 13 Sep 2021
 *      Author: dario
 */

#ifndef LWIP_STACK_INIT_H_
#define LWIP_STACK_INIT_H_

extern void lwip_stack_init();

#endif /* LWIP_STACK_INIT_H_ */
