# Changelog
il formato del file si basa su [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),



## [2.0.1] - 2022-02-17

### Changed
- modificata gestione dimmer, con il suo valore che influenza direttamente i valori RGB passati al ring: ad esempio, scrivo RGB(100, 100, 100), e se il dimmer è a 50 passo RGB(50, 50, 50)

### Added

### Removed




## [2.0.0] - 2022-02-09

### Changed
- bug fix: risolto bug perdita connettività dopo scollegamento e ricollegamento rete

### Added
- registro dimmer per gestire la luminosità dei pixel più velocemente
- colori e ON/OFF gestibili anche per singoli pixel

### Removed
- rimossa modalità single pixel ON




## [1.0.3] - 2022-01-19

### Changed
- bug fix: modificate soglie per riconoscere movimenti jogger

### Added

### Removed




## [1.0.2] - 2021-12-22

### Changed
- indirizzo ip di default impostato a 192.168.21.1
- bug fix: scheda NXP non rispondeva a ping se alimentata singolarmente

### Added

### Removed




## [1.0.1] - 2021-12-16

### Changed
- indirizzo ip di default impostato a 192.168.2.201
- bug fix: inversione tra movimenti jogger e porzione di ring illuminata

### Added

### Removed




## [1.0.0] - 2021-11-24

### Changed
- primo rilascio

### Added

### Removed